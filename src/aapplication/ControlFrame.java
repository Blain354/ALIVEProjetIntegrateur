package aapplication;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import file.BluetoothConfigurationFileModifier;
import file.LanguageFrame;
import platooning.CarInfoPanel;
import platooning.Vehicle;
import platooning.VehicleType;

/**
 * This class contains all the application that controls all the cars
 * 
 * @author Olivier St-Pierre
 * @author Guillaume Blain
 */
public class ControlFrame {

	private JFrame frame;
	private ArrayList<CarInfoPanel> carInfoPanelList = new ArrayList<CarInfoPanel>();
	private JPanel panel;
	public static ArrayList<VehiculeFrame> vehicleFrameList = new ArrayList<VehiculeFrame>();
	// Translating variables
	private Locale currentLocale;
	private ResourceBundle texts;
	private String lang="";


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ControlFrame window = new ControlFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ControlFrame() {
		try {
			initialize();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws IOException
	 */
	private void initialize() throws IOException {
		initializeLanguage();
		ImageIcon icon = new ImageIcon(ImageIO.read(new File("resources\\alivecar.png")));
		frame = new JFrame();
		frame.setIconImage(icon.getImage());
		panel = new JPanel();
		frame.setTitle(texts.getString("Title"));

		frame.setBounds(100, 100, 720, 720);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu mnOptions = new JMenu(texts.getString("Options"));
		menuBar.add(mnOptions);

		JMenuItem mntmChangeLang = new JMenuItem(texts.getString("languageSettings"));
		mntmChangeLang.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new LanguageFrame();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		mnOptions.add(mntmChangeLang);

		JMenuItem mntmConfigurationBT = new JMenuItem(texts.getString("Configuration"));
		mntmConfigurationBT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new BluetoothConfigurationFileModifier();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});	
		mnOptions.add(mntmConfigurationBT);

		JMenuItem mntmHelpBtn = new JMenuItem(texts.getString("Help"));
		mntmHelpBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				HelpFrame help = new HelpFrame();
				help.setVisible(true);
			}			
		});
		mnOptions.add(mntmHelpBtn);
		
		JMenuItem mntmPropos = new JMenuItem(texts.getString("about"));
		mnOptions.add(mntmPropos);
		
		JMenuItem mntmQuit = new JMenuItem(texts.getString("quit"));
		mntmQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnOptions.add(mntmQuit);
		mntmPropos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JOptionPane.showMessageDialog(null, texts.getString("AboutMessage"), texts.getString("about"), 1);
			}			
		});


		JComboBox<String> comboBoxComPort = new JComboBox<String>();
		menuBar.add(comboBoxComPort);
		comboBoxComPort.setSelectedIndex(comboBoxComPort.getItemCount() - 1);

		BufferedReader br = new BufferedReader(new FileReader(new File("resources/comPortConfig.txt")));
		String line = br.readLine();
		while (!(line.equals("$"))) {
			comboBoxComPort.addItem(line);
			line = br.readLine();
		}
		br.close();
		// *****************************

		JButton addCarbtn = new JButton(texts.getString("AddDevicesMessage"));
		addCarbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {


				String comport;
				comport = (String) comboBoxComPort.getSelectedItem();
				comport = comport.substring(comport.indexOf(':') + 1);
				comport = "com" + comport;
				VehiculeFrame vehicleFrame = new VehiculeFrame(new Vehicle(VehicleType.valueOf(getCarName((String) comboBoxComPort.getSelectedItem()))), comport);
				vehicleFrameList.add(vehicleFrame);
				CarInfoPanel carInfoPanel = new CarInfoPanel(vehicleFrame.getTitle(),vehicleFrame);
				carInfoPanelList.add(carInfoPanel);
				panel.add( carInfoPanel,BorderLayout.CENTER);
				panel.setVisible(true);
				frame.revalidate();
			}
		});

		menuBar.add(addCarbtn);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));


		
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));



		// ***********************
	}

	/**
	 * This method get the boot language and initialize the language of the application
	 * 
	 * @throws IOException
	 */
	//Guillaume Blain
	private void initializeLanguage() {
		BufferedReader br = null;
		lang = "";

		try {
			br = new BufferedReader(new FileReader(new File("resources/languageConfig.txt")));
			lang = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		currentLocale = new Locale(lang);
		texts = ResourceBundle.getBundle("TraductionBundle", currentLocale);
	}

	/**
	 * This method will find the name of the car in the comboBox String
	 * @param comboBoxString the string from the comboBox
	 * @return the name of the car
	 */
	//Olivier St-Pierre
	private String getCarName(String comboBoxString) {
		int separatorPosition = comboBoxString.indexOf(':');
		return comboBoxString.substring(0,separatorPosition).toUpperCase();
	}

	public ArrayList<VehiculeFrame> getVehicleFrames(){
		return vehicleFrameList;
	}

}
