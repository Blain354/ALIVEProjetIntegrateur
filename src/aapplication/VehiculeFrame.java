package aapplication;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

import com.fazecast.jSerialComm.SerialPort;

import ai.AIPilot;
import graphics.Graphic;
import listeners.AccelChangedListener;
import listeners.AngleChangedListener;
import listeners.DecisionTookListener;
import listeners.DelayChangedListener;
import listeners.DistanceChangedListener;
import listeners.VoiceDecisionListener;
import mapping.DetectionPanel;
import mapping.Mapping2D;
import platooning.Vehicle;
import speechRecog.SpeechRecognition;
import threads.GetData;
import utilities.TextAreaOutputStream;
import videofeed.VideoFeed;

/**
 * This is the main application of the ALIVE project, it manages the different
 * threads and allows the user to interact with the car.
 * 
 * @author Guillaume Blain
 * @author Olivier St-Pierre
 */
public class VehiculeFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static JTextField txtConsolein;
	private OutputStream outStream;
	private InputStream inStream;
	private JTextField tfLeft;
	private JTextField tfMiddle;
	private JTextField tfRight;
	private JPanel panel_Output;
	private AIPilot ai;
	private boolean autoPilotOn = false;
	private boolean aiOn = false;
	private boolean isPlatooning = false;
	private MultiLayerNetwork MLN;
	private DetectionPanel panelDetection;
	private GetData getData;
	private Mapping2D mappingFrame;
	private CameraTab cameraFrame;
	private String comPort = "com8";
	private JButton btnAutopilotmode;
	private JButton btnPlatooning;
	private JButton btnActivateAi;
	private JScrollPane scrollPane;
	private JLabel lblActualAngle;
	private JLabel lblActualAccel;
	private JLabel lblActualDelay;
	private JScrollBar vbar;
	private SerialPort sp;

	// Translating variables
	private Locale currentLocale;
	private ResourceBundle texts;

	private static long startTime;
	private Graphic graphicSensors, graphicDelay, graphicAccel, graphicAngle;

	private Vehicle vehicle;

	private ArrayList<DecisionTookListener> decisionTookListenerList = new ArrayList<DecisionTookListener>();
	private ArrayList<DistanceChangedListener> distanceChangedListenerList = new ArrayList<DistanceChangedListener>();
	private JPanel panelControl;
	private JPanel panel_Info;
	private SpeechRecognition speech;
	/**
	 * Launch the application.
	 */
	// Guillaume Blain
	public VehiculeFrame(Vehicle vehicle, String comPort) {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				try {
					outStream.close();
					inStream.close();
				} catch (IOException e1) {
					e1.printStackTrace();
					
				}
			}
		});
		this.vehicle = vehicle;
		this.comPort = comPort;
		try {
			createApplication(this.comPort);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		setVisible(true);

		startTime = System.nanoTime();

		sp = SerialPort.getCommPort(this.comPort);
		sp.setComPortParameters(115200, 8, 1, 0);
		sp.setComPortTimeouts(SerialPort.TIMEOUT_WRITE_BLOCKING, 0, 0);

		if (sp.openPort()) {
			System.out.println("Port open");
		} else {
			System.out.println("Port closed");
			return;
		}
		// Loads the network using the .zip file containing the configuration of the
		// trained network
		try {
			MLN = MultiLayerNetwork.load(new File("resources/AI.zip"), false);
		} catch (IOException e) {
			e.printStackTrace();
		}
		speech = new SpeechRecognition();
		speech.addVoiceDecisionListener(new VoiceDecisionListener() {
			public void voiceDecision(int command) {
				getData.sendData(command);
			}
		});
		speech.start();
		outStream = sp.getOutputStream();
		inStream = sp.getInputStream();
		getData = new GetData(inStream, outStream, vehicle);
		getData.start();

		getData.addAccelChangedListener(new AccelChangedListener() {
			public void accelChanged(double accel) {
				graphicAccel.addData(currentTime() * Math.pow(10, -9), accel, 0);
				lblActualAccel.setText(accel + " m/s^2");
			}
		});
		getData.addAngleChangedListener(new AngleChangedListener() {
			public void angleChanged(double angle) {
				graphicAngle.addData(currentTime() * Math.pow(10, -9), angle, 0);
				lblActualAngle.setText(angle + " deg");
			}
		});
		VideoFeed.addDelayChangedListener(new DelayChangedListener() {
			public void delayChanged(double delay) {
				graphicDelay.addData(currentTime() * Math.pow(10, -9), delay, 0);
				lblActualDelay.setText(delay + " s");
			}
		});
		getData.addDistanceChangedListener(new DistanceChangedListener() {
			public void distanceChanged(int left, int forward, int right) throws IOException {

				panelDetection.setDistanceL(left);
				tfLeft.setText(left + " cm");

				panelDetection.setDistanceM(forward);
				tfMiddle.setText(forward + " cm");

				panelDetection.setDistanceR(right);
				tfRight.setText(right + " cm");
				graphicSensors.addData(currentTime() * Math.pow(10, -9), left, 0);
				graphicSensors.addData(currentTime() * Math.pow(10, -9), forward, 1);
				graphicSensors.addData(currentTime() * Math.pow(10, -9), right, 2);

				// lblActualDelay.setText(Utilities.round((double)currentTime()/Math.pow(10,
				// 9),2)+" s");
				// vbar.setValue(vbar.getMaximum());

				try {
					cameraFrame.getSensorFeed().setSensorsLength(left, forward, right);
				}catch(NullPointerException e) {
					
				}
				
				distanceChangedVehiculeFrame(left, forward, right);
			}
		});
		getData.addDecisionTookListener(new DecisionTookListener() {

			@Override
			public void decisionTook(int decision) {
				decisionTookVehiculeFrame(decision);
			}

		});
	}

	/**
	 * Initialize and create the contents of the frame.
	 */
	// Guillaume Blain
	public void createApplication(String comPort) throws IOException {

		initializeLanguage();
		
		ImageIcon icon = new ImageIcon(ImageIO.read(new File("resources\\alivecar.png")));
		setIconImage(icon.getImage());
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(10, 10, 1500, 1000);
		setMinimumSize(new Dimension(1250, 750));
		setTitle(vehicle.getName());
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240, 240, 240));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		panelControl = new JPanel();
		panelControl.requestFocusInWindow();
		SpringLayout sl_contentPane = new SpringLayout();
		sl_contentPane.putConstraint(SpringLayout.NORTH, panelControl, 0, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, panelControl, 0, SpringLayout.WEST, contentPane);
		contentPane.setLayout(sl_contentPane);
		panelControl.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true),
				texts.getString("controlPanel"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panelControl);
		panelControl.setLayout(null);

		JButton btnUp = new JButton("Up");
		btnUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});

		btnUp.setBounds(155, 22, 70, 70);
		btnUp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				associateImageWithButton(btnUp, "Up2.png", 0);
			}

			public void mouseExited(MouseEvent e) {
				associateImageWithButton(btnUp, "Up.png", 0);
			}

			public void mousePressed(MouseEvent e) {
				associateImageWithButton(btnUp, "Up.png", 0);
				send(1);
			}

			public void mouseReleased(MouseEvent e) {
				associateImageWithButton(btnUp, "Up2.png", 0);
				send(0);
			}
		});
		panelControl.add(btnUp);

		JButton btnDown = new JButton("Down");
		btnDown.requestFocusInWindow();
		btnDown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnDown.setBounds(155, 186, 70, 70);
		btnDown.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				associateImageWithButton(btnDown, "Up2.png", 2);
			}

			public void mouseExited(MouseEvent e) {
				associateImageWithButton(btnDown, "Up.png", 2);
			}

			public void mousePressed(MouseEvent e) {
				associateImageWithButton(btnDown, "Up.png", 2);
				send(3);
			}

			public void mouseReleased(MouseEvent e) {
				associateImageWithButton(btnDown, "Up2.png", 2);
				send(0);
			}
		});
		panelControl.add(btnDown);

		JButton btnRight = new JButton("Right");
		btnRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnRight.setBounds(243, 105, 70, 70);
		btnRight.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				associateImageWithButton(btnRight, "Up2.png", 1);
			}

			public void mouseExited(MouseEvent e) {
				associateImageWithButton(btnRight, "Up.png", 1);
			}

			public void mousePressed(MouseEvent e) {
				associateImageWithButton(btnRight, "Up.png", 1);
				send(2);
			}

			public void mouseReleased(MouseEvent e) {
				associateImageWithButton(btnRight, "Up2.png", 1);
				send(0);
			}
		});
		panelControl.add(btnRight);

		JButton btnLeft = new JButton("left");
		btnLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnLeft.setBounds(67, 105, 70, 70);
		btnLeft.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				associateImageWithButton(btnLeft, "Up2.png", 3);
			}

			public void mouseExited(MouseEvent e) {
				associateImageWithButton(btnLeft, "Up.png", 3);
			}

			public void mousePressed(MouseEvent e) {
				associateImageWithButton(btnLeft, "Up.png", 3);
				send(4);
			}

			public void mouseReleased(MouseEvent e) {
				associateImageWithButton(btnLeft, "Up2.png", 3);
				send(0);
			}
		});
		panelControl.add(btnLeft);

		JCheckBox chckbxModeClavier = new JCheckBox(texts.getString("keyboardControl"));
		chckbxModeClavier.setSelected(false);
		chckbxModeClavier.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				chckbxModeClavier.setSelected(false);
			}
		});
		chckbxModeClavier.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (chckbxModeClavier.isEnabled()) {
					int c = e.getKeyCode();
					if (c == KeyEvent.VK_UP) {
						send(1);
						vehicle.setRolling(true);
					} else if (c == KeyEvent.VK_RIGHT) {
						send(2);
					} else if (c == KeyEvent.VK_DOWN) {
						send(3);
						vehicle.setReversing(true);
					} else if (c == KeyEvent.VK_LEFT) {
						send(4);
					}
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (chckbxModeClavier.isEnabled()) {
					send(0);
					vehicle.setRolling(false);
					vehicle.setReversing(false);
				}
			}

		});
		chckbxModeClavier.setBounds(6, 249, 321, 23);
		panelControl.add(chckbxModeClavier);

		JButton btnUpLeft = new JButton("upLeft");
		btnUpLeft.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				associateImageWithButton(btnUpLeft, "UpAngle2.png", 3);
			}

			public void mouseExited(MouseEvent e) {
				associateImageWithButton(btnUpLeft, "UpAngle.png", 3);
			}

			public void mousePressed(MouseEvent e) {
				associateImageWithButton(btnUpLeft, "UpAngle.png", 3);
				send(14);
			}

			public void mouseReleased(MouseEvent e) {
				associateImageWithButton(btnUpLeft, "UpAngle2.png", 3);
				send(0);
			}
		});
		btnUpLeft.setBounds(87, 44, 40, 40);
		panelControl.add(btnUpLeft);

		JButton btnUpRight = new JButton("upRight");
		btnUpRight.setBounds(250, 44, 40, 40);
		btnUpRight.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				associateImageWithButton(btnUpRight, "UpAngle2.png", 0);
			}

			public void mouseExited(MouseEvent e) {
				associateImageWithButton(btnUpRight, "UpAngle.png", 0);
			}

			public void mousePressed(MouseEvent e) {
				associateImageWithButton(btnUpRight, "UpAngle.png", 0);
				send(12);
			}

			public void mouseReleased(MouseEvent e) {
				associateImageWithButton(btnUpRight, "UpAngle2.png", 0);
				send(0);
			}
		});
		panelControl.add(btnUpRight);

		JButton btnDownLeft = new JButton("downLeft");
		btnDownLeft.setBounds(87, 196, 40, 40);
		btnDownLeft.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				associateImageWithButton(btnDownLeft, "UpAngle2.png", 2);
			}

			public void mouseExited(MouseEvent e) {
				associateImageWithButton(btnDownLeft, "UpAngle.png", 2);
			}

			public void mousePressed(MouseEvent e) {
				associateImageWithButton(btnDownLeft, "UpAngle.png", 2);
				send(34);
			}

			public void mouseReleased(MouseEvent e) {
				associateImageWithButton(btnDownLeft, "UpAngle2.png", 2);
				send(0);
			}
		});
		panelControl.add(btnDownLeft);

		JButton btnDownRight = new JButton("downRight");
		btnDownRight.setBounds(250, 196, 40, 40);
		btnDownRight.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				associateImageWithButton(btnDownRight, "UpAngle2.png", 1);
			}

			public void mouseExited(MouseEvent e) {
				associateImageWithButton(btnDownRight, "UpAngle.png", 1);
			}

			public void mousePressed(MouseEvent e) {
				associateImageWithButton(btnDownRight, "UpAngle.png", 1);
				send(32);
			}

			public void mouseReleased(MouseEvent e) {
				associateImageWithButton(btnDownRight, "UpAngle2.png", 1);
				send(0);
			}
		});
		panelControl.add(btnDownRight);

		associateImageWithButton(btnUp, "Up.png", 0);
		associateImageWithButton(btnRight, "Up.png", 1);
		associateImageWithButton(btnDown, "Up.png", 2);
		associateImageWithButton(btnLeft, "Up.png", 3);
		associateImageWithButton(btnUpRight, "UpAngle.png", 0);
		associateImageWithButton(btnDownRight, "UpAngle.png", 1);
		associateImageWithButton(btnDownLeft, "UpAngle.png", 2);
		associateImageWithButton(btnUpLeft, "UpAngle.png", 3);

		JPanel panelBtn = new JPanel();
		panelBtn.setBounds(91, 278, 197, 104);
		panelControl.add(panelBtn);

		btnAutopilotmode = new JButton(texts.getString("autoPilotButton"));
		btnAutopilotmode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!autoPilotOn) {
					autoPilotOn = true;
					enableButton(btnAutopilotmode);
					btnAutopilotmode.setText(texts.getString("autoPilotButtonOff"));
					send(5);
				} else {
					autoPilotOn = false;
					setEnabledButtonTo(true);
					btnAutopilotmode.setText(texts.getString("autoPilotButton"));
					send(6);
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					send(0);
				}
			}
		});

		btnPlatooning = new JButton(texts.getString("platooningButton"));
		btnPlatooning.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!isPlatooning) {
					isPlatooning = true;
					try {
						getData.setPlatooning(isPlatooning);
					} catch (IOException e) {
						e.printStackTrace();
					}
					btnPlatooning.setText(texts.getString("platooningOff"));
					enableButton(btnPlatooning);
				} else {
					isPlatooning = false;
					try {
						getData.setPlatooning(isPlatooning);
					} catch (IOException e) {
						e.printStackTrace();
					}
					btnPlatooning.setText(texts.getString("platooningButton"));
					setEnabledButtonTo(true);
				}

			}
		});
		FlowLayout fl_panelBtn = new FlowLayout(FlowLayout.CENTER, 5, 5);
		panelBtn.setLayout(fl_panelBtn);
		panelBtn.add(btnPlatooning);
		panelBtn.add(btnAutopilotmode);

		btnActivateAi = new JButton(texts.getString("aiButton"));
		panelBtn.add(btnActivateAi);

		JLabel lblSpeed = new JLabel(texts.getString("lblSpeed"));
		lblSpeed.setBounds(101, 393, 185, 14);
		panelControl.add(lblSpeed);
		lblSpeed.setHorizontalAlignment(SwingConstants.CENTER);

		JSlider sliderSpeed = new JSlider();
		sliderSpeed.setMajorTickSpacing(10);
		sliderSpeed.setMinorTickSpacing(10);
		sliderSpeed.setPaintTicks(true);
		sliderSpeed.setBounds(25, 413, 336, 31);
		panelControl.add(sliderSpeed);
		sliderSpeed.setValue(130);
		sliderSpeed.setMaximum(250);
		sliderSpeed.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				send(33);
				send(sliderSpeed.getValue());
			}
		});

		btnActivateAi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!aiOn) {
					aiOn = true;
					try {
						ai = new AIPilot(getData, MLN, vehicle);

					} catch (IOException e) {
					}
					ai.setAiState(true);
					btnActivateAi.setText(texts.getString("aiButtonOff"));
					enableButton(btnActivateAi);
				} else {
					aiOn = false;
					ai.setAiState(false);
					setEnabledButtonTo(true);
					btnActivateAi.setText(texts.getString("aiButton"));
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					send(0);
				}
			}
		});

		JPanel panelConsole = new JPanel();
		sl_contentPane.putConstraint(SpringLayout.SOUTH, panelControl, -1, SpringLayout.NORTH, panelConsole);
		sl_contentPane.putConstraint(SpringLayout.NORTH, panelConsole, 470, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, panelConsole, 0, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, panelConsole, -1, SpringLayout.SOUTH, contentPane);
		getContentPane().add(panelConsole);
		SpringLayout sl_panelConsole = new SpringLayout();
		panelConsole.setLayout(sl_panelConsole);

		scrollPane = new JScrollPane();
		sl_panelConsole.putConstraint(SpringLayout.NORTH, scrollPane, 0, SpringLayout.NORTH, panelConsole);
		sl_panelConsole.putConstraint(SpringLayout.WEST, scrollPane, 0, SpringLayout.WEST, panelConsole);
		sl_panelConsole.putConstraint(SpringLayout.EAST, scrollPane, 0, SpringLayout.EAST, panelConsole);
		scrollPane.setAutoscrolls(true);
		panelConsole.add(scrollPane);

		vbar = scrollPane.getVerticalScrollBar();
		JTextArea txtrConsoleout = new JTextArea();

		scrollPane.setViewportView(txtrConsoleout);
		txtrConsoleout.setEditable(false);
		txtrConsoleout.setBackground(Color.WHITE);

		PrintStream out = new PrintStream(new TextAreaOutputStream(txtrConsoleout));

		// redirect standard output stream to the TextAreaOutputStream 1
		System.setOut(out);

		// redirect standard error stream to the TextAreaOutputStream
		System.setErr(out);

		txtConsolein = new JTextField();
		sl_panelConsole.putConstraint(SpringLayout.SOUTH, scrollPane, -5, SpringLayout.NORTH, txtConsolein);
		sl_panelConsole.putConstraint(SpringLayout.NORTH, txtConsolein, -30, SpringLayout.SOUTH, panelConsole);
		sl_panelConsole.putConstraint(SpringLayout.WEST, txtConsolein, 10, SpringLayout.WEST, panelConsole);
		sl_panelConsole.putConstraint(SpringLayout.EAST, txtConsolein, -118, SpringLayout.EAST, panelConsole);
		sl_panelConsole.putConstraint(SpringLayout.SOUTH, txtConsolein, -5, SpringLayout.SOUTH, panelConsole);
		txtConsolein.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					int num = 0;
					try {
						num = Integer.parseInt(txtConsolein.getText());
						send(num);
					} catch (NumberFormatException e1) {
						System.out.println(texts.getString("NumErrMessage"));
					}
					System.out.println(txtConsolein.getText());
					txtConsolein.setText("");
					vbar.setValue(vbar.getMaximum());
				}
			}
		});
		panelConsole.add(txtConsolein);
		txtConsolein.setColumns(10);

		JButton btnSend = new JButton(texts.getString("Send"));
		sl_panelConsole.putConstraint(SpringLayout.NORTH, btnSend, 0, SpringLayout.NORTH, txtConsolein);
		sl_panelConsole.putConstraint(SpringLayout.WEST, btnSend, 6, SpringLayout.EAST, txtConsolein);
		sl_panelConsole.putConstraint(SpringLayout.SOUTH, btnSend, 31, SpringLayout.SOUTH, scrollPane);
		sl_panelConsole.putConstraint(SpringLayout.EAST, btnSend, -10, SpringLayout.EAST, panelConsole);
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int num = 0;
				try {
					num = Integer.parseInt(txtConsolein.getText());
					send(num);
				} catch (NumberFormatException e) {
					System.out.println(texts.getString("NumErrMessage"));
				}
				System.out.println(txtConsolein.getText());
				txtConsolein.setText("");
			}
		});
		panelConsole.add(btnSend);

		panel_Info = new JPanel();
		sl_contentPane.putConstraint(SpringLayout.SOUTH, panel_Info, -1, SpringLayout.NORTH, panelConsole);
		sl_contentPane.putConstraint(SpringLayout.EAST, panelConsole, 0, SpringLayout.EAST, panel_Info);
		sl_contentPane.putConstraint(SpringLayout.EAST, panelControl, -1, SpringLayout.WEST, panel_Info);

		JLabel lblNewLabel = new JLabel("0%");
		lblNewLabel.setBounds(25, 444, 46, 14);
		panelControl.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("100%");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setBounds(325, 445, 46, 14);
		panelControl.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("50%");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(170, 444, 46, 14);
		panelControl.add(lblNewLabel_2);
		sl_contentPane.putConstraint(SpringLayout.WEST, panel_Info, 388, SpringLayout.WEST, contentPane);
		contentPane.add(panel_Info);
		panel_Info.setLayout(null);

		JLabel lblAngle = new JLabel(texts.getString("lblAngle"));
		lblAngle.setBounds(10, 14, 60, 14);
		panel_Info.add(lblAngle);

		lblActualAngle = new JLabel("0 deg");
		lblActualAngle.setBounds(99, 14, 76, 14);
		panel_Info.add(lblActualAngle);

		JLabel lblAcceleration = new JLabel(texts.getString("lblAcceleration"));
		lblAcceleration.setBounds(185, 14, 92, 14);
		panel_Info.add(lblAcceleration);

		lblActualAccel = new JLabel("0 m/s^2");
		lblActualAccel.setBounds(274, 14, 76, 14);
		panel_Info.add(lblActualAccel);

		JLabel lblDelay = new JLabel(texts.getString("DelayGraphName"));
		lblDelay.setBounds(10, 40, 60, 14);
		panel_Info.add(lblDelay);

		lblActualDelay = new JLabel("0 ms");
		lblActualDelay.setBounds(99, 39, 76, 14);
		panel_Info.add(lblActualDelay);
		panel_Output = new JPanel();
		sl_contentPane.putConstraint(SpringLayout.EAST, panel_Info, -1, SpringLayout.WEST, panel_Output);
		sl_contentPane.putConstraint(SpringLayout.NORTH, panel_Output, 0, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, panel_Output, 791, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, panel_Output, 0, SpringLayout.EAST, contentPane);

		panel_Output.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), texts.getString("outputPanel"),
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panel_Output);
		panel_Output.setLayout(new BorderLayout(0, 0));

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		panel_Output.add(tabbedPane);

		graphicDelay = new Graphic(0, 1000, 0, 30000);
		tabbedPane.addTab(texts.getString("DelayGraphName"), null, graphicDelay, null);

		graphicSensors = new Graphic(3, 110);
		tabbedPane.addTab(texts.getString("SensorGraphName"), null, graphicSensors, null);
		tabbedPane.setEnabledAt(1, true);

		graphicAccel = new Graphic(0, 30000);
		tabbedPane.addTab(texts.getString("AccelGraphName"), null, graphicAccel, null);

		graphicAngle = new Graphic(0, 200);
		tabbedPane.addTab(texts.getString("AngleGraphName"), null, graphicAngle, null);

		JPanel detection_panel = new JPanel();
		sl_contentPane.putConstraint(SpringLayout.SOUTH, detection_panel, -80, SpringLayout.NORTH, panelConsole);
		sl_contentPane.putConstraint(SpringLayout.NORTH, panel_Info, 1, SpringLayout.SOUTH, detection_panel);
		sl_contentPane.putConstraint(SpringLayout.NORTH, detection_panel, 0, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, detection_panel, 388, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, detection_panel, -1, SpringLayout.WEST, panel_Output);
		detection_panel.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)),
				texts.getString("detectionPanel"), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		getContentPane().add(detection_panel);
		detection_panel.setLayout(null);

		tfLeft = new JTextField();
		detection_panel.add(tfLeft);
		tfLeft.setBounds(36, 21, 86, 27);
		tfLeft.setEditable(false);
		tfLeft.setColumns(10);

		tfMiddle = new JTextField();
		detection_panel.add(tfMiddle);
		tfMiddle.setBounds(158, 21, 86, 27);
		tfMiddle.setEditable(false);
		tfMiddle.setColumns(10);

		tfRight = new JTextField();
		tfRight.setBounds(280, 21, 86, 27);
		detection_panel.add(tfRight);
		tfRight.setEditable(false);
		tfRight.setColumns(10);
		panelDetection = new DetectionPanel();
		panelDetection.setBounds(16, 54, 370, 326);
		detection_panel.add(panelDetection);
		panelDetection.repaint();

		JPanel panelMapping2D = new JPanel();
		sl_contentPane.putConstraint(SpringLayout.WEST, panelMapping2D, 791, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, panelMapping2D, -1, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, panelMapping2D, -1, SpringLayout.EAST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, panel_Output, -1, SpringLayout.NORTH, panelMapping2D);
		sl_contentPane.putConstraint(SpringLayout.NORTH, panelMapping2D, 473, SpringLayout.NORTH, contentPane);
		panelMapping2D.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), texts.getString("Mapping2DPanel"),
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelMapping2D.setLayout(new BorderLayout(0, 0));
		mappingFrame = new Mapping2D();
		panelMapping2D.add(mappingFrame);
		getContentPane().add(panelMapping2D);
		
		this.vehicle.addMapping(mappingFrame);
		
		mappingFrame.addVehicle(this.vehicle);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnCamera = new JMenu(texts.getString("menuCamera"));
		mnCamera.addMenuListener(new MenuListener() {
			public void menuCanceled(MenuEvent e) {
			}

			public void menuDeselected(MenuEvent e) {
			}

			public void menuSelected(MenuEvent e) {
				cameraFrame = new CameraTab();
				cameraFrame.setVisible(true);
				cameraFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			}
		});
		menuBar.add(mnCamera);

	}

	/**
	 * this method associate an image to a specific button
	 * 
	 * @param mainButton the button to associate the image
	 * @param imageFile  the file of the image being in resources
	 * @param nbRotation numbers of rotations of the image on the button
	 */
	// Guillaume Blain
	public void associateImageWithButton(JButton mainButton, String imageFile, int nbRotation) {
		Image imgRead = null;
		try {
			imgRead = ImageIO.read(new File("resources/" + imageFile));
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Erreur pendant la lecture du fichier d'image " + imageFile);
		}

		Image imgRedim = imgRead.getScaledInstance(mainButton.getWidth(), mainButton.getHeight(), Image.SCALE_SMOOTH);
		BufferedImage img = toBufferedImage(imgRedim);
		for (int i = 0; i < nbRotation; i++) {
			img = rotateCw(img);
		}

		mainButton.setOpaque(false);
		mainButton.setContentAreaFilled(false);
		mainButton.setBorderPainted(false);

		mainButton.setText("");
		mainButton.setIcon(new ImageIcon(img));

		imgRead.flush();
		imgRedim.flush();
	}

	/**
	 * this method rotate a bufferedImage clock-wise
	 * 
	 * @param img
	 * @return the rotated image
	 */
	// Guillaume Blain
	public BufferedImage rotateCw(BufferedImage img) {
		int width = img.getWidth();
		int height = img.getHeight();
		BufferedImage newImage = new BufferedImage(height, width, img.getType());

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				newImage.setRGB(height - 1 - j, i, img.getRGB(i, j));
			}
		}
		return newImage;
	}

	/**
	 * this method convert an Image to a bufferedImage
	 * 
	 * @param img
	 * @return the converted bufferedImage
	 */
	// Guillaume Blain
	public static BufferedImage toBufferedImage(Image img) {
		if (img instanceof BufferedImage) {
			return (BufferedImage) img;
		}
		BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage(img, 0, 0, null);
		bGr.dispose();
		return bimage;
	}

	/**
	 * this method send a value to the outStream
	 * 
	 * @param val
	 */
	// Guillaume Blain
	public void send(int val) {
		try {
			outStream.write(val);
			outStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Problem writing");
		}

	}

	/**
	 * This method allows to wait
	 * 
	 * @param delay
	 */
	// Guillaume Blain
	public void await(int delay) {
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to change the language of the application
	 * 
	 * @throws IOException
	 */
	// Guillaume Blain
	private void initializeLanguage() {
		currentLocale = new Locale(getBootLanguage());
		texts = ResourceBundle.getBundle("TraductionBundle", currentLocale);
	}

	/**
	 * This method returns which language is selected which is used when creating
	 * the frame
	 * 
	 * @return the selected language
	 */
	//Guillaume Blain
	public String getBootLanguage() {
		BufferedReader br = null;
		String lang = "";

		try {
			br = new BufferedReader(new FileReader(new File("resources/languageConfig.txt")));
			lang = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lang;
	}

	/**
	 * This method gives how much time has been passed by the launch of the
	 * application
	 * 
	 * @return how much time has been passed by the launch of the application
	 */
	// Olivier St-Pierre
	public static long currentTime() {
		return System.nanoTime() - startTime;
	}

	/**
	 * This method is used to set a specific group of button enable or not
	 * 
	 * @param isEnabled the state of the buttons
	 */
	// Guillaume Blain
	private void setEnabledButtonTo(boolean isEnabled) {
		btnActivateAi.setEnabled(isEnabled);
		btnAutopilotmode.setEnabled(isEnabled);
		btnPlatooning.setEnabled(isEnabled);
	}

	/**
	 * This method is used to activate only one button and desactivate the others
	 * 
	 * @param buttonToEnabled the selected button to enable
	 */
	// Guillaume Blain
	private void enableButton(JButton buttonToEnabled) {
		btnActivateAi.setEnabled(buttonToEnabled.equals(btnActivateAi));
		btnAutopilotmode.setEnabled(buttonToEnabled.equals(btnAutopilotmode));
		btnPlatooning.setEnabled(buttonToEnabled.equals(btnPlatooning));
	}

	
	/**
	 * adds a listener to the list
	 * 
	 * @param listener the listener to add
	 */
	// Olivier St-Pierre
	public void addDecisionTookListener(DecisionTookListener listener) {
		decisionTookListenerList.add(listener);
	}

	
	/**
	 * adds a listener to the list
	 * 
	 * @param listener the listener to add
	 */
	// Olivier St-Pierre
	public void addDistanceChangedListener(DistanceChangedListener listener) {
		distanceChangedListenerList.add(listener);
	}

	/**
	 * Call the method of the listener
	 * @throws IOException 
	 */
	// Olivier St-Pierre
	public void distanceChangedVehiculeFrame(int left, int forward, int right) throws IOException {
		for (DistanceChangedListener listener : distanceChangedListenerList) {
			listener.distanceChanged(left, forward, right);
		}
	}

	/**
	 * Call the method of the listener
	 */
	// Olivier St-Pierre
	public void decisionTookVehiculeFrame(int decision) {
		for (DecisionTookListener listener : decisionTookListenerList) {
			listener.decisionTook(decision);
		}
	}

	public void setComPort(String newComPort) {
		comPort = newComPort;
	}

	public OutputStream getOutStream() {
		return outStream;
	}

	public void setOutStream(OutputStream outStream) {
		this.outStream = outStream;
	}

	public InputStream getInStream() {
		return inStream;
	}

	public JTextField getTfLeft() {
		return tfLeft;
	}

	public void setTfLeft(JTextField tfLeft) {
		this.tfLeft = tfLeft;
	}

	public JTextField getTfMiddle() {
		return tfMiddle;
	}

	public void setTfMiddle(JTextField tfMiddle) {
		this.tfMiddle = tfMiddle;
	}

	public JTextField getTfRight() {
		return tfRight;
	}

	public void setTfRight(JTextField tfRight) {
		this.tfRight = tfRight;
	}

	public boolean getautoPilotOn() {
		return autoPilotOn;
	}

	public boolean getAiOn() {
		return aiOn;
	}

	public AIPilot getAi() {
		return ai;
	}

	public Mapping2D getMap() {
		return mappingFrame;
	}

	public SerialPort getSP() {
		return this.sp;
	}

	public Vehicle getVehicle() {
		return this.vehicle;
	}
}