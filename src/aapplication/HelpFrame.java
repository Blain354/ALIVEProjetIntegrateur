package aapplication;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

/**
 * A window with the user guide on it
 * @author Caroline Houle
 * @author Guillaume Blain
 * @author Olivier St-Pierre
 */
public class HelpFrame extends JFrame {

	private JPanel contentPane;
	private static final long serialVersionUID = 1L;
	private JButton btnPagePrecedente;
	private JButton btnPageSuivante;

	// c'est ici que l'on declare un tableau ou on enumere toutes les pages d'aide desirees
	private String tableauImages[];
	private String title;

	// Translating variables
	private Locale currentLocale;
	private ResourceBundle texts;

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	
	/*
	 * Par Caroline Houle
	 */
	public HelpFrame()  {
		initializeLanguage();
		ImageIcon icon = null;
		try {
			icon = new ImageIcon(ImageIO.read(new File("resources\\alivecar.png")));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		setIconImage(icon.getImage());
		tableauImages=createImagesFolder();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 886, 808);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		title=texts.getString("UserGuideTitle");

		// creation du composant qui contiendra les pages d'aide
		ImagesAvecDefilement panAide = new ImagesAvecDefilement();
		//Pour modifier la largeur et la couleur du cadre autour des pages 
		panAide.setLargeurCadre(10);
		panAide.setBackground(Color.white); 
		panAide.setFichiersImages(this.tableauImages); // on precise quelles images seront utilisees
		panAide.setBounds(49, 88, 772, 606);
		contentPane.add(panAide);



		btnPagePrecedente = new JButton(texts.getString("PreviousPage"));
		btnPagePrecedente.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnPagePrecedente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnPagePrecedente.setEnabled( panAide.precedente() );
				btnPageSuivante.setEnabled(true);
			}
		});
		btnPagePrecedente.setBounds(49, 705, 165, 45);
		contentPane.add(btnPagePrecedente);

		btnPageSuivante = new JButton(texts.getString("NextPage"));
		btnPageSuivante.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnPageSuivante.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnPageSuivante.setEnabled( panAide.suivante() );
				btnPagePrecedente.setEnabled(true);
			}

		});

		if (tableauImages.length==1 ) {
			btnPagePrecedente.setEnabled(false);
			btnPageSuivante.setEnabled(false);
		}
		btnPageSuivante.setBounds(656, 705, 165, 45);
		contentPane.add(btnPageSuivante);

		JLabel lblAideConcepts = new JLabel(title);
		lblAideConcepts.setHorizontalAlignment(SwingConstants.CENTER);
		lblAideConcepts.setForeground(Color.WHITE);
		lblAideConcepts.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblAideConcepts.setBounds(264, 11, 342, 34);
		contentPane.add(lblAideConcepts);


	}
	/**
	 * This method get the boot language and initialize the language of the
	 * application
	 * 
	 * @throws IOException
	 */
	// Guillaume Blain
	private void initializeLanguage() {
		BufferedReader br = null;
		String lang = "";

		try {
			br = new BufferedReader(new FileReader(new File("resources/languageConfig.txt")));
			lang = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		currentLocale = new Locale(lang);
		texts = ResourceBundle.getBundle("TraductionBundle", currentLocale);
	}

	/**
	 * This method gets the name of the page for the user guide
	 * @return the array of String with the name of the pages
	 */
	//Olivier St-Pierre
	public String[] createImagesFolder() {
		String[] temp = new String[4];
		temp[0]=texts.getString("UserGuide1");
		temp[1]=texts.getString("UserGuide2");
		temp[2]=texts.getString("UserGuide3");
		temp[3]=texts.getString("UserGuide4");
		return temp;
	}
}
