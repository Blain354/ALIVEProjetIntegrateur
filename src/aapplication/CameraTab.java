package aapplication;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SpringLayout;

import file.FileWindow;
import listeners.CameraNotFoundListener;
import videofeed.VideoFeed;
import videofeed.VideoFeedType;
/**
 * This class contains a frame with all the different types of videoFeed we use
 * @author Guillaume Blain
 *
 */
public class CameraTab extends JFrame{

	private static final long serialVersionUID = 2566902767245836325L;
	private FileWindow fileWindowImage, fileWindowAI;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JLabel lblAI;
	private VideoFeed boxFeed;
	private VideoFeed sensorFeed;
	private VideoFeed videoFeed;
	private VideoFeed onlyBoxFeed;
	private JPanel panelSetup;
	private boolean errorCalled = false;

	// Translating variables
	private Locale currentLocale;
	private ResourceBundle texts;

	/**
	 * Create the application.
	 * @throws IOException 
	 */
	public CameraTab()  {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws IOException 
	 */

	private void initialize() {
		initializeLanguage();

		ImageIcon icon = null;
		try {
			icon = new ImageIcon(ImageIO.read(new File("resources\\alivecar.png")));
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		setIconImage(icon.getImage());
		this.setBounds(100, 100, 718, 700);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle(texts.getString("CameraTab"));

		fileWindowImage = new FileWindow();
		fileWindowAI = new FileWindow();

		videoFeed = new VideoFeed(VideoFeedType.VideoFeed, texts.getString("videoFeed"));
		boxFeed = new VideoFeed(VideoFeedType.BoxFeed, texts.getString("boxFeed"));
		onlyBoxFeed = new VideoFeed(VideoFeedType.OnlyBoxFeed, texts.getString("onlyBoxFeed"));
		sensorFeed = new VideoFeed(VideoFeedType.SensorFeed, texts.getString("sensorFeed"));
		VideoFeed.addCameraNotFoundListener(new CameraNotFoundListener() {

			@Override
			public void cameraNotFound() {
				if(!errorCalled) {
					JOptionPane.showMessageDialog(null, texts.getString("errorCamera"), texts.getString("titleError"), JOptionPane.ERROR_MESSAGE);
					errorCalled=true;
				}

			}
		});

		

		fileWindowImage.addWindowListener(new WindowAdapter() {
			@Override
			public void windowDeactivated(WindowEvent e) {
				File file = fileWindowImage.getFileChooser().getSelectedFile();
				BufferedImage img = null;
				try {
					img = ImageIO.read(file);
					videoFeed.setImage(img);
					boxFeed.setImage(img);
					onlyBoxFeed.setImage(img);
					sensorFeed.setImage(img);
				} catch (IOException e1) {
				}

			}
		});

		fileWindowAI.addWindowListener(new WindowAdapter() {
			@Override
			public void windowDeactivated(WindowEvent e) {
				File file = fileWindowAI.getFileChooser().getSelectedFile();
				String modelName = null;
				modelName = file.getPath();
				boxFeed.setAI(modelName);
				onlyBoxFeed.setAI(modelName);
				lblAI.setText(file.getName());
			}
		});
		SpringLayout springLayout = new SpringLayout();
		getContentPane().setLayout(springLayout);

		panelSetup = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panelSetup, 10, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, panelSetup, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panelSetup, 93, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panelSetup, 403, SpringLayout.WEST, getContentPane());
		getContentPane().add(panelSetup);
		panelSetup.setLayout(null);

		JButton btnChooseImage = new JButton(texts.getString("ChooseImage"));
		btnChooseImage.setBounds(10, 4, 179, 21);
		panelSetup.add(btnChooseImage);

		JButton btnAiChooser = new JButton(texts.getString("ChooseAI"));
		btnAiChooser.setBounds(199, 4, 109, 21);
		panelSetup.add(btnAiChooser);
		
		
		
		JRadioButton rdbtnCameraStream = new JRadioButton(texts.getString("Camera"));
		rdbtnCameraStream.setBounds(10, 29, 103, 21);
		panelSetup.add(rdbtnCameraStream);
		
		rdbtnCameraStream.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				videoFeed.start();
				boxFeed.start();
				sensorFeed.start();
				onlyBoxFeed.start();
				btnChooseImage.setEnabled(false);
			}
		});
		buttonGroup.add(rdbtnCameraStream);
		
		JRadioButton rdbtnFileChooser = new JRadioButton(texts.getString("ChooseFile"));
		rdbtnFileChooser.setBounds(10, 54, 103, 21);
		panelSetup.add(rdbtnFileChooser);
		rdbtnFileChooser.setSelected(true);
		rdbtnFileChooser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				videoFeed.stop();
				boxFeed.stop();
				sensorFeed.stop();
				onlyBoxFeed.stop();
				btnChooseImage.setEnabled(true);
			}
		});
		buttonGroup.add(rdbtnFileChooser);

		lblAI = new JLabel(boxFeed.getAIName());
		lblAI.setBounds(119, 29, 264, 21);
		panelSetup.add(lblAI);

		JPanel panelFeed = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panelFeed, 104, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, panelFeed, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panelFeed, -10, SpringLayout.SOUTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panelFeed, -10, SpringLayout.EAST, getContentPane());
		getContentPane().add(panelFeed);
		panelFeed.setLayout(new GridLayout(2, 2, 20, 20));

		panelFeed.add(videoFeed);


		panelFeed.add(boxFeed);

		panelFeed.add(onlyBoxFeed);

		panelFeed.add(sensorFeed);

		btnAiChooser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fileWindowAI.setVisible(true);
			}
		});
		btnChooseImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fileWindowImage.setVisible(true);
			}
		});
	}

	public VideoFeed getSensorFeed() {
		return sensorFeed;
	}

	/**
	 * This method get the boot language and initialize the language of the
	 * application
	 * 
	 * @throws IOException
	 */
	private void initializeLanguage() {
		BufferedReader br = null;
		String lang = "";

		try {
			br = new BufferedReader(new FileReader(new File("resources/languageConfig.txt")));
			lang = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		currentLocale = new Locale(lang);
		texts = ResourceBundle.getBundle("TraductionBundle", currentLocale);
	}

}
