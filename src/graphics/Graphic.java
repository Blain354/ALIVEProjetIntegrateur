package graphics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.util.ArrayList;

import javax.swing.JPanel;

/**
 * This class contains all the necessary to create an adaptable graphic
 * 
 * @author Olivier St-Pierre
 *
 */
public class Graphic extends JPanel {

	private static final long serialVersionUID = 1L;
	private double xMin = 0;
	private double xMax = 10;

	private final double YCAP;

	private double yMax = 100;
	private double yMin = -1;

	private double tickValueX;
	private double tickValueY;

	private final int NB_TICKS_Y = 10;
	private final int NB_TICKS_X = 10;

	private final int TICK_LENGTH_HALF = 5;

	private double originY;

	private double pixelPerUnitX;
	private double pixelPerUnitY;

	private final int OFFSET_X = 38;

	private final int OFFSET_X_LABEL_X = 5;
	private final int OFFSET_Y_LABEL_X = -15;

	private final int OFFSET_X_LABEL_Y = 38;
	private final int OFFSET_Y_LABEL_Y = 10;

	private AffineTransform atr;

	private Path2D axis, ticks;

	private ArrayList<Function> functions = new ArrayList<Function>();

	/**
	 * Create the panel.
	 */
	public Graphic(int numberOfFunctions, double YCAP) {
		this.YCAP = YCAP;
		createArrayList(numberOfFunctions);
	}

	/**
	 * Creates a new Graphic
	 * @param yMin              the min on the y axis
	 * @param yMax              the max on the yAxis
	 * @param numberOfFunctions the number of function to add to the function
	 * @param YCAP              the cap in the y axis
	 */
	public Graphic(double yMin, double yMax, int numberOfFunctions, double YCAP) {
		this.yMin = yMin;
		this.yMax = yMax;
		this.YCAP = YCAP;
		setLayout(null);

		createArrayList(numberOfFunctions);
	}

	/**
	 * The method called to paint the panel
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		atr = new AffineTransform();

		axis = new Path2D.Double();
		ticks = new Path2D.Double();

		pixelPerUnitX = (getWidth() - OFFSET_X) / (xMax - xMin);
		pixelPerUnitY = (getHeight()) / (yMax - yMin);

		tickValueX = (xMax - xMin) / NB_TICKS_X;
		tickValueY = (yMax - yMin) / NB_TICKS_Y;

		originY = yMax * pixelPerUnitY;

		atr.scale(pixelPerUnitX, -pixelPerUnitY);

		g2d.translate(OFFSET_X, originY);

		g2d.drawString(yMax + "", 0, (int) (pixelPerUnitY * yMax));

		createAxis();

		createFunctions();

		g2d.draw(atr.createTransformedShape(axis));

		drawFunctions(g2d, atr);
		g2d.setColor(Color.black);

		createTicks(g2d);
		g2d.draw(ticks);

		g2d.setColor(Color.black);
	}

	/**
	 * Create the axis
	 */
	private void createAxis() {
		axis.moveTo(0, 0);
		axis.lineTo(xMax, 0);

		axis.moveTo(0, yMin);
		axis.lineTo(0, yMax);

	}

	/**
	 * Creates all the ticks on the axis and their scaling
	 * 
	 * @param g2d the object used to draw the ticks
	 */
	private void createTicks(Graphics2D g2d) {
		for (int i = 0; i <= NB_TICKS_X; i++) {
			ticks.moveTo(i * tickValueX * pixelPerUnitX, -TICK_LENGTH_HALF);
			ticks.lineTo(i * tickValueX * pixelPerUnitX, TICK_LENGTH_HALF);
			g2d.drawString(Math.floor(i * tickValueX) + "",
					(int) (i * tickValueX * pixelPerUnitX) - OFFSET_X_LABEL_X, OFFSET_Y_LABEL_X);
		}
		for (int j = 1; j <= NB_TICKS_Y; j++) {
			ticks.moveTo(-TICK_LENGTH_HALF, -(yMin + j * tickValueY) * pixelPerUnitY);
			ticks.lineTo(TICK_LENGTH_HALF, -(yMin + j * tickValueY) * pixelPerUnitY);
			g2d.drawString(Math.floor(j * tickValueY) + "", -OFFSET_X_LABEL_Y,
					(int) (-(yMin + j * tickValueY) * pixelPerUnitY) + OFFSET_Y_LABEL_Y);

		}
	}

	/**
	 * Adds all the functions to the arrayList
	 * 
	 * @param numberOfFunctions the number of functions that is shown on the graph
	 */
	private void createArrayList(int numberOfFunctions) {
		for (int i = 0; i <= numberOfFunctions; i++) {
			functions.add(new Function());
		}
	}

	/**
	 * This method creates all the functions from the graph
	 */
	private void createFunctions() {
		for (int j = 0; j < functions.size(); j++) {
			functions.get(j).createFunction(pixelPerUnitX, pixelPerUnitY, xMin);
		}
	}

	/**
	 * This method allows the user to add a new data to the graphic
	 * 
	 * @param x the x value
	 * @param y the y value
	 * @param function the function of the graphic
	 */
	public void addData(double x, double y, int function) {
		if (y > yMax && y <= YCAP) {
			yMax = y;
		} else if (y < yMin) {
			yMin = y;
		}
		if (y > YCAP) {
			y = YCAP;
			yMax = YCAP;
		}
		if (x > xMax) {
			xMax = x;
		}
		functions.get(function).getValues().add(new Point2D(x, y));
		repaint();
	}

	/**
	 * This method draws all the functions on the panel
	 * 
	 * @param g2d the g2d that is used to draw
	 * @param atr the matrix of transformation with the scaling
	 */
	public void drawFunctions(Graphics2D g2d, AffineTransform atr) {
		for (int i = 0; i < functions.size(); i++) {
			if (i == 0) {
				g2d.setColor(Color.red);
			} else if (i == 1) {
				g2d.setColor(Color.green);
			} else if (i == 2) {
				g2d.setColor(Color.blue);
			}
			g2d.draw(atr.createTransformedShape(functions.get(i).getPath()));
		}
	}

}
