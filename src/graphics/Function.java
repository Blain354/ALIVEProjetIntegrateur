package graphics;

import java.awt.geom.Path2D;
import java.util.ArrayList;

/**
 * This class contains all the points from a function and methods to draw it
 * @author Olivier St-Pierre
 *
 */
public class Function {

	private ArrayList<Point2D> values;
	private Path2D path;
	/**
	 * Create a new function
	 */
	public Function() {
		values = new ArrayList<Point2D>();
	}

	/**
	 * Create the path based on the points
	 * @param pixelPerUnitX the pixel per unit in x of the graph
	 * @param pixelPerUnitY the pixel per unit in y of the graph
	 * @param xMin the min in x of the graph
	 */
	public void createFunction(double pixelPerUnitX, double pixelPerUnitY, double xMin) {
		path = new Path2D.Double();
		path.moveTo(0, 0);
		for(int i=0; i<values.size(); i++) {
			if(values.get(i).getX()>=xMin) {
				path.lineTo(values.get(i).getX(), values.get(i).getY());
				path.moveTo(values.get(i).getX(), values.get(i).getY());
			}

		}
	}

	public ArrayList<Point2D> getValues(){
		return values;
	}

	public Path2D getPath() {
		return path;
	}
}
