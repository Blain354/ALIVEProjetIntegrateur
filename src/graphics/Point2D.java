package graphics;

/**
 * This class contains the x and y coordinate of a point
 * @author Olivier St-Pierre
 *
 */
public class Point2D {

	private double x;
	private double y;

	/**
	 * Create a new Point2D
	 * @param x the x value
	 * @param y the y value
	 */
	public Point2D(double x, double y) {
		this.x =x;
		this.y =y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}


}
