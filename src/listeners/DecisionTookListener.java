package listeners;
/**
 * This interface contains the method for a DecisionListener
 * @author Olivier St-Pierre
 *
 */

import java.util.EventListener;
/**
 * This interface includes the decisionTook method that will be call when a car takes a decision
 * @author Olivier St-Pierre
 *
 */
public interface DecisionTookListener extends EventListener{
	public void decisionTook(int decision);
}
