package listeners;

import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;

/**
 * This class is identical to the ScoreIterationListener class from dl4j, but the loggers have been changed to System out.
 * @author dl4j
 * @author Maximilien Harvey
 *
 */
public class CustomScoreIterationListener extends ScoreIterationListener {
	private static final long serialVersionUID = -6306919675012214048L;
	private int printIterations = 10;

	/**
	 * Basic constructor based on frequency with which to print scores
	 * @param printIterations frequency with which to print scores (i.e., every
	 *                        printIterations parameter updates)
	 */
	/*
	 * dl4j
	 */
	public CustomScoreIterationListener(int printIterations) {
		this.printIterations = printIterations;
	}

	/** Default constructor printing every 10 iterations */
	/*
	 * dl4j
	 */
	public CustomScoreIterationListener() {
	}

	/**
	 * Prints the score after a certain amount of iterations
	 * 
	 * @param model The model on which to evaluate the score
	 * @param iteration The iteration on which the model is training
	 * @param epoch The epoch on which the model is training
	 */
	/*
	 * Maximilien Harvey
	 */
	public void iterationDone(Model model, int iteration, int epoch) {
		if (printIterations <= 0)
			printIterations = 1;
		if (iteration % printIterations == 0) {
			double score = model.score();
			System.out.println("Score at iteration {" + iteration + "} is {" + score + "}");
		}
	}

	/**
	 * Returns a string for printing useful information about this object
	 * 
	 * @return A string for printing useful information about this object
	 */
	/*
	 * dl4j
	 */
	public String toString() {
		return "ScoreIterationListener(" + printIterations + ")";
	}
}
