package listeners;

import java.util.EventListener;
/**
 * interface that implements a DistanceChanged method
 * @author Guillaume Blain
 *
 */
public interface DelayChangedListener extends EventListener{
	public void delayChanged(double delay);
}

