package listeners;

import java.util.EventListener;
/**
 * interface that implements a DistanceChanged method
 * @author Guillaume Blain
 *
 */
public interface AccelChangedListener extends EventListener{
	public void accelChanged(double accelleration);
}
