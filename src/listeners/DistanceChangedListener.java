package listeners;

import java.io.IOException;
import java.util.EventListener;
/**
 * interface that implements a DistanceChanged method
 * @author Guillaume Blain
 *
 */
public interface DistanceChangedListener extends EventListener{
	public void distanceChanged(int left, int forward, int right) throws IOException;
}
