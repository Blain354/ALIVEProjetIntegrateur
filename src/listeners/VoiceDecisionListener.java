package listeners;

import java.util.EventListener;
/**
 * This is the listener for the speech recognition
 * @author �mile Gagn�
 */
public interface VoiceDecisionListener extends EventListener{
	public void voiceDecision(int decision);
}
