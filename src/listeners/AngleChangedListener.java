package listeners;

import java.util.EventListener;
/**
 * interface that implements a DistanceChanged method
 * @author Guillaume Blain
 *
 */
public interface AngleChangedListener extends EventListener{
	public void angleChanged(double angle);
}
