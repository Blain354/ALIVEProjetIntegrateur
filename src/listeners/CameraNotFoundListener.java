package listeners;
/**
 * This interface implements the cameraNotFound method that will be called when the program cannot access the camere
 * @author Olivier St-Pierre
 *
 */
public interface CameraNotFoundListener {
	public void cameraNotFound();
}
