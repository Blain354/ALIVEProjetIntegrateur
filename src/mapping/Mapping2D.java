package mapping;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.JPanel;

import platooning.Vehicle;
import utilities.DisplayModel;

/**
 * This class will draw all the position of the cars on the same panel
 * @author Olivier St-Pierre
 *
 */
public class Mapping2D extends JPanel {

	private static final long serialVersionUID = -4652705085854314829L;

	private final double CAR_WIDTH = 0.20;

	private Vehicle defaultCar;
	private double realWidth=2;

	private boolean firstTime = true;
	private boolean zoom = false;
	private boolean positionOn = false;
	private DisplayModel display;
	
	private final int ABBERATE_DISTANCE = 30;
	private final double OBSTACLES_WIDTH = 0.03;


	private Scale scale;
	private Rectangle2D car;

	//variables for the drag
	private double originX;
	private double originY;

	private double originXMouse;
	private double originYMouse;

	private ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
	private ArrayList<Ellipse2D> obstaclesMerged = new ArrayList<Ellipse2D>();
	private DecimalFormat df = new DecimalFormat("#.##");


	/**
	 * Create a new MApping2D and a new DrawPanel associate with it
	 */
	public Mapping2D() {
		setLayout(null);

		scale = new Scale(display);
		scale.setBounds(570, 375, 65, 50);
		add(scale);
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				originXMouse=e.getX();
				originYMouse=e.getY();
				if(positionOn) {
					positionOn = false;
				}else {
					positionOn=true;
				}
				
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				originX+=(e.getX()-originXMouse);
				originY+=(e.getY()-originYMouse);
				originXMouse=e.getX();
				originYMouse=e.getY();
				repaint();
			}
		});
		addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent e) {
				int nbTurn = e.getWheelRotation();
				zoom = true;
				realWidth+=nbTurn;
				if(realWidth<=0) realWidth=1;
				repaint();
			}
		});
		this.setVisible(true);
	}

	/**
	 * Add a point to the map
	 * @param posX the x coordinate of the point
	 * @param posY the y coordinate of the point
	 */
	//jamais utilise
	public void mergeMap() {
		obstaclesMerged.clear();
		for(Vehicle vehicle: vehicles) {
			obstaclesMerged.addAll(vehicle.getObstacles());
		}
		
	}	

	/**
	 * The function called when you repaint the component
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		
		
		
		if(firstTime) {
			display = new DisplayModel(getWidth(), getHeight(), realWidth);	
			scale.setAffine(display);
			instanceOrigin();
			defaultCar = vehicles.get(0);
			firstTime=false;
		}
		if(zoom) {
			display = new DisplayModel(getWidth(), getHeight(), realWidth);	
			scale.setAffine(display);
		}

		AffineTransform mat = display.getModel();


		g2d.translate(originX, originY);
		Color colorPreCar = g2d.getColor();
		for(Vehicle vehicle : defaultCar.getBroadcastVehicles()) {
			car =  new Rectangle2D.Double(vehicle.getPositionX()-CAR_WIDTH/2, vehicle.getPositionY()-CAR_WIDTH/2, CAR_WIDTH, CAR_WIDTH);
			g2d.setColor(vehicle.getVehicleType().getColor());
			g2d.fill(mat.createTransformedShape(car));
		}
		g2d.setColor(colorPreCar);

		g2d.setColor(Color.black);
		for(Ellipse2D e : defaultCar.getObstacles()) {
			g2d.fill(mat.createTransformedShape(e));
			if(positionOn) {
				g2d.drawString("("+df.format(e.getCenterX())+","+df.format(e.getCenterY())+")", Math.round(mat.getScaleX()*e.getCenterX()), Math.round(mat.getScaleY()*e.getCenterY()));
			}
			
		}

	}
	/**
	 * This method instance the origin of the map at the center of the panel
	 */
	public void instanceOrigin() {
		originX=getWidth()/2;
		originY=getHeight()/2;
	}


	public void addVehicle(Vehicle vehicle) {
		vehicles.add(vehicle);
		repaint();
	}
	
	
	public void setPositionOn(boolean positionOn) {
		this.positionOn = positionOn;
	}


}


