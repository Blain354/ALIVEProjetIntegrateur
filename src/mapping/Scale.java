package mapping;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;

import javax.swing.JPanel;

import utilities.DisplayModel;

/**
 * This JPanel can be put above a Mapping to show the scaling of the map
 * @author Olivier St-Pierre
 *
 */
public class Scale extends JPanel {

	
	private static final long serialVersionUID = 1L;
	private DisplayModel display;
	Path2D scale;

	private final double X_TOP_LEFT = 5;
	private final double Y_TOP_LEFT = 20;

	private final double X_BOTTOM_LEFT = 5;
	private final double Y_BOTTOM_LEFT = 30;

	private final double X_BOTTOM_RIGHT = 50;
	private final double Y_BOTTOM_RIGHT = 30;

	private final double X_TOP_RIGHT = 50;
	private final double Y_TOP_RIGHT = 20;

	private final double DECAL_Y=15;

	/**
	 * Create a new Scale
	 * @param display the DisplayModel of the Map
	 */
	public Scale(DisplayModel display) {
		this.display=display;
	}
	/**
	 * The method call to paint the panel
	 */
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		scale = new Path2D.Double();
		scale.moveTo(X_TOP_LEFT	, Y_TOP_LEFT);
		scale.lineTo(X_BOTTOM_LEFT, Y_BOTTOM_LEFT);
		scale.lineTo(X_BOTTOM_RIGHT,Y_BOTTOM_RIGHT);
		scale.lineTo(X_TOP_RIGHT,Y_TOP_RIGHT);


		g2d.draw(scale);

		g2d.drawString(utilities.Utilities.round(Math.pow(display.getModel().getScaleX()/(X_TOP_RIGHT-X_TOP_LEFT),-1),4)+" m", (int)X_BOTTOM_LEFT, (int)(Y_BOTTOM_LEFT+DECAL_Y));
	}

	public void setAffine(DisplayModel display) {
		this.display=display;
		repaint();
	}

}
