package mapping;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.JPanel;

import utilities.DisplayModel;

/**
 * This class handles the position of the car and the obstacles detected
 * @author Olivier St-Pierre
 *
 */
public class Mapping2DOriginal extends JPanel {

	private static final long serialVersionUID = -4652705085854314829L;

	private ArrayList<Ellipse2D.Double> points = new ArrayList<Ellipse2D.Double>();
	private ArrayList<Ellipse2D.Double> path = new ArrayList<Ellipse2D.Double>();

	private boolean isRolling = false;
	private double posX=0;
	private double posY=0;

	//with a speed of 110
	private final double MAXSPEED = 0.4;

	private final double CAR_WIDTH = 0.20;
	private final int ABBERATE_DISTANCE = 30;
	private final double OBSTACLES_WIDTH = 0.03;
	private double timeS;
	private double timeEnd = 0;
	private double time = 0;

	private double realWidth=2;

	private boolean firstTime = true;
	private boolean zoom = false;
	private DisplayModel display;


	private Scale scale;
	private Rectangle2D car;

	//variables for the drag
	private double originX;
	private double originY;

	private double originXMouse;
	private double originYMouse;

	/**
	 * Create a new MApping2D and a new DrawPanel associate with it
	 */
	public Mapping2DOriginal() {
		setLayout(null);

		scale = new Scale(display);
		scale.setBounds(570, 375, 65, 50);
		add(scale);
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				originXMouse=e.getX();
				originYMouse=e.getY();
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				originX+=(e.getX()-originXMouse);
				originY+=(e.getY()-originYMouse);
				originXMouse=e.getX();
				originYMouse=e.getY();
				repaint();
			}
		});
		addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent e) {
				int nbTurn = e.getWheelRotation();
				zoom = true;
				realWidth+=nbTurn;
				if(realWidth<=0) realWidth=1;
				repaint();
			}
		});
		this.setVisible(true);
	}

	/**
	 * Add a point to the map
	 * @param posX the x coordinate of the point
	 * @param posY the y coordinate of the point
	 */
	public void addPoint(double posX, double posY) {
		points.add(new Ellipse2D.Double(posX, posY, OBSTACLES_WIDTH,OBSTACLES_WIDTH));
		repaint();
	}
	/**
	 * Add a point to the path
	 * @param posX the x coordinate of the point
	 * @param posY the y coordinate of the point
	 */
	public void addPath(double posX, double posY) {
		path.add(new Ellipse2D.Double(posX, posY, OBSTACLES_WIDTH,OBSTACLES_WIDTH));
		repaint();
	}


	/**
	 * Calculate the position of the car
	 * @param angle the angle of the car in degree
	 */
	public void trackPosition(int angle) {
		if(isRolling) {
			double angleRad = Math.toRadians(angle);
			if(timeEnd == 0) {
				timeEnd = System.nanoTime();
			}
			time = System.nanoTime()-timeEnd;
			timeS = time*Math.pow(10, -9);
			double distance = MAXSPEED*timeS;
			posX+= distance*Math.sin(angleRad);
			posY+= distance*Math.cos(angleRad);
			timeEnd = System.nanoTime();
			addPath(posX, posY);
			if(posX>=realWidth/2||-posX>=realWidth/2||posY>=display.getHeightRealUnit()/2||-posY>=display.getHeightRealUnit()/2) {
				zoom=true;
				realWidth+=1;
			}
			repaint();
		}else {
			time = 0;
			timeEnd = 0;
		}
	}

	/**
	 * Calculate the position of an obstacle based on the distances from the sensors
	 * @param distanceLeft the distance from the left sensor
	 * @param distanceCenter the distance from the middle sensor
	 * @param distanceRight the distance from the right sensor
	 * @param angle the angle in degree
	 */
	public void wallDetection(int distanceLeft, int distanceCenter, int distanceRight, int angle) {
		double posXWall = 0;
		double posYWall = 0;
		double angleRad = Math.toRadians(angle);
		if(distanceLeft<ABBERATE_DISTANCE) {
			posXWall = posX+distanceLeft/100.0*Math.sin(-angleRad+Math.toRadians(60));
			posYWall = posY+distanceLeft/100.0*Math.cos(-angleRad+Math.toRadians(60));
			addPoint(posXWall, posYWall);
		}
		if(distanceCenter<ABBERATE_DISTANCE) {
			posXWall = posX+distanceCenter/100.0*Math.sin(angleRad);
			posYWall = posY+distanceCenter/100.0*Math.cos(angleRad);
			addPoint(posXWall, posYWall);
		}
		if(distanceRight<ABBERATE_DISTANCE) {
			posXWall = posX+distanceCenter/100.0*Math.sin(-angleRad+Math.toRadians(60));
			posYWall = posY+distanceCenter/100.0*Math.cos(-angleRad+Math.toRadians(60));
			addPoint(posXWall, posYWall);
		}

	}

	/**
	 * The function called when you repaint the component
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		if(firstTime) {
			display = new DisplayModel(getWidth(), getHeight(), realWidth);	
			scale.setAffine(display);
			instanceOrigin();
			firstTime=false;
		}
		if(zoom) {
			display = new DisplayModel(getWidth(), getHeight(), realWidth);	
			scale.setAffine(display);
		}

		AffineTransform mat = display.getModel();


		g2d.translate(originX, originY);

		car =  new Rectangle2D.Double(posX-CAR_WIDTH/2, posY-CAR_WIDTH/2, CAR_WIDTH, CAR_WIDTH);
		g2d.setColor(Color.red);
		g2d.fill(mat.createTransformedShape(car));
		for(Ellipse2D e : path) {
			g2d.fill(mat.createTransformedShape(e));
		}
		g2d.setColor(Color.black);
		for(Ellipse2D e : points) {
			g2d.fill(mat.createTransformedShape(e));
		}

	}

	/**
	 * Set the current state of the car
	 * @param isRolling true if the car is rolling false if not
	 */
	public void setRolling(boolean isRolling) {
		this.isRolling = isRolling;
	}
	/**
	 * This method instance the origin of the map at the center of the panel
	 */
	public void instanceOrigin() {
		originX=getWidth()/2;
		originY=getHeight()/2;
	}




}

