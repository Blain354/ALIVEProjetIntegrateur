
package mapping;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import geometry.GraphicVector;
import utilities.DisplayModel;

/**
 * This class extends JPanel and it display an image of the ALIVE car with three vectors representing the distances of each vector 
 * @author Guillaume Blain
 */
public class DetectionPanel extends JPanel {

	private static final long serialVersionUID = -4057020340831196682L;
	private Image imgRead;
	private double width;
	GraphicVector gvM;
	GraphicVector gvL;
	GraphicVector gvR;
	DisplayModel display;
	private boolean firstTime = false;

	/**
	 * initialize the DetectionPanel by creating three GraphicVector and the image of the car 
	 * @throws IOException
	 */
	public DetectionPanel() throws IOException {
		width = 100;
		gvL = new GraphicVector(140,135,-150);
		gvM = new GraphicVector(184, 125,-90);
		gvR = new GraphicVector(225,135,-30);
		imgRead = ImageIO.read(new File("resources/carUpView.png"));
		imgRead = imgRead.getScaledInstance((int) width,(int)(width * 1.3),Image.SCALE_SMOOTH);
		repaint();
	}

	/**
	 * is responsible for the graphic representation of the DetectionPanel
	 * param g Graphics
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		if(!firstTime) {
			display = new DisplayModel(getWidth(), getHeight(), 0.5);
		}

		g2d.drawImage(imgRead,getWidth()/2 - imgRead.getWidth(null)/2,125,(int) width,(int)(width * 1.3),null);
		gvM.dessiner(g2d);
		gvL.dessiner(g2d);
		gvR.dessiner(g2d);
	}

	/**
	 * Set the distance of the left captor
	 * @param left
	 */
	public void setDistanceL(int left) {
		gvL.setLength(left);
		repaint();
	}
	
	/**
	 * Set the distance of the left captor
	 * @param middle
	 */
	public void setDistanceM(int middle ) {
		gvM.setLength(middle);
		repaint();
	}

	/**
	 * Set the distance of the left captor
	 * @param right
	 */
	public void setDistanceR(int right) {
		gvR.setLength(right);
		repaint();
	}


}
