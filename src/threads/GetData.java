package threads;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import org.jsoup.Jsoup;

import listeners.AccelChangedListener;
import listeners.AngleChangedListener;
import listeners.DecisionTookListener;
import listeners.DistanceChangedListener;
import platooning.Vehicle;
import platooning.VehicleType;
import utilities.Utilities;
import videofeed.VideoFeed;

/**
 * This thread is created and used when the Autopilot is on, to update the data
 * of the sensors on the Application
 * 
 * @author �mile Gagn�
 * @author Olivier St-Pierre
 */
public class GetData extends Thread {

	private InputStream inStream;
	private OutputStream outStream;
	private int angleSign, angle, nbHalfTurn, a, d, g, decision;
	private double accel;
	private Vehicle vehicle;
	private ArrayList<DistanceChangedListener> distanceChangedListenerList = new ArrayList<DistanceChangedListener>();
	private ArrayList<AccelChangedListener> accelChangedListenerList = new ArrayList<AccelChangedListener>();
	private ArrayList<AngleChangedListener> angleChangedListenerList = new ArrayList<AngleChangedListener>();
	private ArrayList<DecisionTookListener> decisionTookListenerList = new ArrayList<DecisionTookListener>();
	private boolean platooning = false;
	
	/**
	 * getData class constructor
	 * @param inputStream InputStream of the current connection
	 * @param outputStream OutputStream of the current connection
	 * @param vehicle Connected vehicle
	 */
	public GetData(InputStream inputStream, OutputStream outputStream, Vehicle vehicle) {
		inStream = inputStream;
		outStream = outputStream;
		this.vehicle = vehicle;
	}
	/**
	 * Run method for the getData thread
	 */
	// �mile Gagn�
	@Override
	public void run() {
		while (true) {
			a = getData(100);
			//System.out.println("avant = " + a);
			g = getData(101);
			//System.out.println("gauche = " + g);
			d = getData(102);
			//System.out.println("droite = " + d); 
			try {
				distanceChanged();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			angleSign = getData(103);
			if(vehicle.getVehicleType().equals(VehicleType.GREENIE)) {
				angle = 180;
			}else {
				angle = getData(104);
			}
			
			nbHalfTurn = getData(105);
			angleChanged();
			decision = getData(20);
			decisionTook();
			//			String temp = getDataString(69);
			//			System.out.println(temp);
			if (platooning) {
				sendPlatooning();
			}
			
			vehicle.trackPosition(Utilities.calculateAngle(angle, nbHalfTurn, angleSign));
			vehicle.wallDetection(g, a, d, Utilities.calculateAngle(angle, nbHalfTurn, angleSign));
			vehicle.broadcast();
		}
	}

	/**
	 * Main method used to retrieve one answer from the Arduino
	 * 
	 * @param command Number under 255 to identify a command to send to the Arduino
	 * @return Returns the number read (The answer)
	 */
	// �mile Gagn�
	public int getData(int command) {
		try {
			outStream.write(command);
			outStream.flush();

			while (inStream.available() == 0);
			return Integer.parseInt(Jsoup.parse(inStream.read() + "").text());
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("problem sending/receiving");
			return 0;
		}
	}

	public void sendData(int command) {
		try {
			outStream.write(command);
			outStream.flush();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Adds a listener to the thread
	 * 
	 * @param listener
	 */
	// �mile Gagn�
	public void addDistanceChangedListener(DistanceChangedListener listener) {
		distanceChangedListenerList.add(listener);
	}

	/**
	 * Updates all the listeners
	 * @throws IOException 
	 */
	// �mile Gagn�
	public void distanceChanged() throws IOException {
		for (DistanceChangedListener listener : distanceChangedListenerList) {
			listener.distanceChanged(g, a, d);
		}
	}

	
	/**
	 * Adds a accellistener to the thread
	 * 
	 * @param listener the accelListener
	 */
	// Olivier St-Pierre
	public void addAccelChangedListener(AccelChangedListener listener) {
		accelChangedListenerList.add(listener);
	}

	
	/**
	 * Update all the listeners
	 */
	// Olivier St-Pierre
	public void accelChanged() {
		for (AccelChangedListener listener : accelChangedListenerList) {
			listener.accelChanged(accel);
		}
	}

	
	/**
	 * Adds all the angleListener to the thread
	 * 
	 * @param listener the angle listener
	 */
	// Olivier St-Pierre
	public void addAngleChangedListener(AngleChangedListener listener) {
		angleChangedListenerList.add(listener);
	}

	
	/**
	 * Update all the angle listener
	 */
	// Olivier St-Pierre
	public void angleChanged() {
		for (AngleChangedListener listener : angleChangedListenerList) {
			listener.angleChanged(Utilities.calculateAngle(angle, nbHalfTurn, angleSign));
		}
	}
	
	/**
	 * adds a listener to the list
	 * 
	 * @param listener the listener to add
	 */
	//Olivier St-Pierre
	public void addDecisionTookListener(DecisionTookListener listener) {
		decisionTookListenerList.add(listener);
	}


	
	/**
	 * Call the method of the listener
	 */
	//Olivier St-Pierre
	public void decisionTook() {
		for (DecisionTookListener listener : decisionTookListenerList) {
			listener.decisionTook(decision);
		}
	}

	public void setPlatooning(boolean x) throws IOException {
		this.platooning = x;
		outStream.write(0);
		outStream.flush();
	}

	/**
	 * Sends platooning to the car
	 */
	// �mile Gagn�
	public void sendPlatooning() {
		try {
			outStream.write(Utilities.detectFrontCar(VideoFeed.getCenterX(), 140, 180));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public OutputStream getOutputStream() {
		return this.outStream;
	}

}
