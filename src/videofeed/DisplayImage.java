package videofeed;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * This JPanel display an image in the maximum size possible and keeps his proportion
 * 
 * @author Guillaume Blain
 *
 */
public class DisplayImage extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Image image = null;

	/**
	 * Create the panel.
	 */
	public DisplayImage(Image imageToDisplay) {
		image = imageToDisplay;
	}

	/**
	 * constructor
	 */
	public DisplayImage() {
		try {
			image = ImageIO.read(new File("resources/loading_icon.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Paint component
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		Image img = getScaledImage(image, this);
		int posX = (int) ((this.getWidth() - img.getWidth(null)) / 2);
		int posY = (int) ((this.getHeight() - img.getHeight(null)) / 2);
		g2d.drawImage(img, posX, posY, null);
	}

	/**
	 * this method scale an image in a specific container but it keeps his proportions
	 * @param currentImg the image to display
	 * @param container the reference container where the image is put
	 * @return the scaled image
	 */
	public Image getScaledImage(Image currentImg, Container container) {
		Image imgRedim = null;
		double ratio = 0.0;
		ratio = (double) currentImg.getWidth(null) / (double) currentImg.getHeight(null);
		if (this.getWidth() > currentImg.getWidth(null))
			imgRedim = currentImg.getScaledInstance((int) (container.getHeight() * ratio), container.getHeight(),
					Image.SCALE_SMOOTH);
		else
			imgRedim = currentImg.getScaledInstance(container.getWidth(), (int) (container.getWidth() / ratio),
					Image.SCALE_SMOOTH);

		return imgRedim;
	}


	/**
	 * set the current image to display
	 * @param image
	 */
	public void setImage(Image image) {
		this.image = image;
		repaint();
	}

}