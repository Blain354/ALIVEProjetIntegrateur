package videofeed;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.layers.objdetect.DetectedObject;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;

import geometry.GraphicVector;
import listeners.CameraNotFoundListener;
import listeners.DelayChangedListener;

/**
 * This class will search the images from the camera based on an URL
 * 
 * @author Olivier St-Pierre
 * @author Maximilien Harvey
 *
 */
public class VideoFeed extends JPanel implements Runnable {

	private static final long serialVersionUID = 1L;
	private ComputationGraph model;

	private String modelFileName = "resources\\TinyObstacleAINoPretrainedBlocs1-6_72.zip";
	private NativeImageLoader imageLoader;
	private INDArray mat, results;
	private org.deeplearning4j.nn.layers.objdetect.Yolo2OutputLayer yout;
	private List<DetectedObject> objs;
	private static int x1;
	private static int x2;
	private static long startTime, deltaTime;
	private static ArrayList<DelayChangedListener> DelayChangedListenerList = new ArrayList<DelayChangedListener>();
	private static ArrayList<CameraNotFoundListener> cameraNotFoundListenerList = new ArrayList<CameraNotFoundListener>();

	public static final Color RED = new Color(255, 0, 0);
	public static final Color GREEN = new Color(0, 255, 0);
	public static final Color BLUE = new Color(0, 0, 255);
	public static final Color YELLOW = new Color(255, 255, 0);
	public static final Color CYAN = new Color(0, 255, 255);
	public static final Color MAGENTA = new Color(255, 0, 255);
	public static final Color ORANGE = new Color(255, 128, 0);
	public static final Color PINK = new Color(255, 192, 203);
	public static final Color LIGHTBLUE = new Color(153, 204, 255);
	public static final Color VIOLET = new Color(238, 130, 238);
	Color[] colormap = { RED, BLUE, GREEN, CYAN, YELLOW, MAGENTA, ORANGE, PINK, LIGHTBLUE, VIOLET };

	private VideoFeedType videoFeedType;
	private String name;
	private BufferedImage image = null;
	private long sleepTime = 1;
	private boolean run = false;
	private boolean testAI = false;
	private Thread thread;
	private BufferedImage DEFAULT_IMAGE;
	private BufferedImage NOT_APPLICABLE;
	private final float NOIRCEUR = 0.15f;
	private final double FACTEUR_AI = 32;
	private final double DETECTION_THRESHOLD = 0.5;
	private final String URL_CAMERA = "http://192.168.0.162/capture";

	// SensorFeed variables
	private double gvLLength = 0;
	private double gvMLength = 0;
	private double gvRLength = 0;
	private final double SENSOR_ANGLE_L = -150;
	private final double SENSOR_ANGLE_M = -90;
	private final double SENSOR_ANGLE_R = -30;

	private final int POSX_TXT = 2;
	private final int POSY_TXT = 15;

	/**
	 * Create a new VideoFeed
	 * 
	 * @param videoFeedType the type of the video
	 */
	/*
	 * par Olivier St-Pierre
	 */
	public VideoFeed(VideoFeedType videoFeedType, String name) {

		this.name = name;
		this.videoFeedType = videoFeedType;
		imageLoader = new NativeImageLoader();
		try {
			DEFAULT_IMAGE = ImageIO.read(new File("resources/defaultImage.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			NOT_APPLICABLE = ImageIO.read(new File("resources/notApplicable.png"));
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		try {
			model = ComputationGraph.load(new File(modelFileName), true);
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.image = DEFAULT_IMAGE;

	}

	/**
	 * The thread of the class
	 */
	@Override
	/*
	 * par Olivier St-Pierre
	 */
	public void run() {
			while (run) {
				URL url = null;
				try {
					url = new URL(URL_CAMERA);
					image = ImageIO.read(url);
					testAI = true;
					Thread.sleep(sleepTime);
				} catch (InterruptedException | IOException e) {

					if (this.videoFeedType.equals(VideoFeedType.VideoFeed))
						cameraNotFound();
				}
				try {
					mat = imageLoader.asMatrix(image);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				repaint();
				try {
					Thread.sleep(sleepTime);
				} catch (InterruptedException e) {
					e.printStackTrace();

				}
			}
	}

	/**
	 * The paint method that draw the current image
	 */

	/*
	 * par Maximilien Harvey
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		AffineTransform mat = g2d.getTransform();
		if (image != null) {
			resetScaling(g2d, mat);
			imageScaling(g2d, image);

			if (videoFeedType.equals(VideoFeedType.OnlyBoxFeed) && testAI) {

				RescaleOp op = new RescaleOp(NOIRCEUR, 0, null);
				BufferedImage tempImage = op.filter(image, null);
				g2d.drawImage(tempImage, 0, 0, null);
			} else {

				g2d.drawImage(image, 0, 0, null);
			}
			if ((videoFeedType.equals(VideoFeedType.BoxFeed) || videoFeedType.equals(VideoFeedType.OnlyBoxFeed))
					&& testAI) {
				startTime = System.currentTimeMillis();
				evaluateAIonImage(image);
				deltaTime = System.currentTimeMillis() - startTime;
				delayChanged();
				if (objs != null) {
					for (DetectedObject obj : objs) {

						// Draws the predicted boxes
						g2d.setColor(colormap[obj.getPredictedClass()]);
						double[] xy1 = obj.getTopLeftXY();
						double[] xy2 = obj.getBottomRightXY();
						int x1 = (int) Math.round(FACTEUR_AI * xy1[0]);
						int x2 = (int) Math.round(FACTEUR_AI * xy2[0]);
						int y1 = (int) Math.round(FACTEUR_AI * xy1[1]);
						int y2 = (int) Math.round(FACTEUR_AI * xy2[1]);
						Rectangle2D.Double rect = new Rectangle2D.Double(x1, y1, x2 - x1, y2 - y1);

						if (videoFeedType.equals(VideoFeedType.BoxFeed)) {
							g2d.draw(rect);
						} else {
							g2d.fill(rect);
						}

					}
				}
			}
			sensorFeed(g2d, mat);
		}
		resetScaling(g2d, mat);
		g2d.setColor(Color.red);
		g2d.setFont(new Font("TimesRoman", Font.PLAIN, 18));
		g2d.drawString(name, POSX_TXT, POSY_TXT);

	}

	/**
	 * This method draws the videoFeed with the distances of the sensors on it
	 * 
	 * @param g2d the g2d used to draw
	 */
	// par Olivier St-Pierre
	public void sensorFeed(Graphics2D g2d, AffineTransform mat) {
		if (videoFeedType.equals(VideoFeedType.SensorFeed)) {

			if (!this.image.equals(DEFAULT_IMAGE)) {
				if (!run) {
					resetScaling(g2d, mat);
					imageScaling(g2d, NOT_APPLICABLE);
					g2d.drawImage(NOT_APPLICABLE, 0, 0, null);
				} else {
					double originX = getWidth() / 2;
					double originY = getHeight();

					AffineTransform atr = g2d.getTransform();
					double scalingX = 1 / atr.getScaleX();
					double scalingY = 1 / atr.getScaleY();
					g2d.scale(scalingX, scalingY);

					GraphicVector gvL = new GraphicVector(originX, originY, gvLLength, SENSOR_ANGLE_L);
					GraphicVector gvM = new GraphicVector(originX, originY, gvMLength, SENSOR_ANGLE_M);
					GraphicVector gvR = new GraphicVector(originX, originY, gvRLength, SENSOR_ANGLE_R);

					gvL.setColor(Color.red);
					gvM.setColor(Color.red);
					gvR.setColor(Color.red);

					gvL.dessiner(g2d);
					gvM.dessiner(g2d);
					gvR.dessiner(g2d);

					g2d.setTransform(atr);
				}

			}

		}

	}

	public void setSensorsLength(double gvLLength, double gvMLength, double gvRLength) {
		this.gvLLength = gvLLength;
		this.gvMLength = gvMLength;
		this.gvRLength = gvRLength;
		repaint();
	}

	/**
	 * Start the thread
	 */

	/*
	 * par Olivier St-Pierre
	 */
	public void start() {
		if (run == false) {
			thread = new Thread(this);
			thread.start();
		}
		run = true;
	}

	/**
	 * Stop the thread
	 */

	/*
	 * par Maximilien Harvey
	 */
	public void stop() {
		run = false;
	}

	public BufferedImage getImage() {
		return this.image;
	}

	public void setImage(BufferedImage img) {
		this.image = img;
		if (image.equals(DEFAULT_IMAGE)) {
			testAI = false;
		} else {
			testAI = true;
		}
		repaint();
	}

	public static int getCenterX() {
		return (x2 + x1) / 2;
	}

	/**
	 * Adds all the angleListener to the thread
	 * 
	 * @param listener the angle listener
	 */

	/*
	 * par Olivier St-Pierre
	 */
	public static void addDelayChangedListener(DelayChangedListener listener) {
		DelayChangedListenerList.add(listener);
	}

	/**
	 * Updates all the listeners
	 */

	/*
	 * par Olivier St-Pierre
	 */
	public void delayChanged() {
		if (deltaTime < 1500) {
			for (DelayChangedListener listener : DelayChangedListenerList) {
				listener.delayChanged(deltaTime);
			}
		}
	}

	/**
	 * Calculate the scaling of the g2d so the image can fit in
	 * 
	 * @param g2d object used to draw
	 */

	/*
	 * par Olivier St-Pierre
	 */
	public void imageScaling(Graphics2D g2d, BufferedImage img) {

		double width = img.getWidth(null);
		double height = img.getHeight(null);
		double widthComposant = this.getWidth();
		double heightComposant = this.getHeight();

		double scaleX = widthComposant / width;
		double scaleY = heightComposant / height;

		g2d.scale(scaleX, scaleY);
	}

	/**
	 * Resets the scaling on the g2d
	 * 
	 * @param g2d
	 */
	/*
	 * par Maximilien Harvey
	 */
	public void resetScaling(Graphics2D g2d, AffineTransform mat) {
		g2d.setTransform(mat);
	}

	/**
	 * Evaluates an AI on an image
	 * 
	 * @param image The image to evaluate the AI on
	 */

	/*
	 * par Maximilien Harvey
	 */
	private void evaluateAIonImage(BufferedImage image) {
		try {
			mat = imageLoader.asMatrix(image);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			DataNormalization scaler = new ImagePreProcessingScaler(0, 1);
			scaler.transform(mat);
			results = model.outputSingle(mat);
			yout = (org.deeplearning4j.nn.layers.objdetect.Yolo2OutputLayer) model.getOutputLayer(0);
			objs = yout.getPredictedObjects(results, DETECTION_THRESHOLD);
		} catch (Exception e2) {
			JOptionPane.showMessageDialog(null, "Image incompatible", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void setAI(String modelName) {
		modelFileName = modelName;

		try {
			model = ComputationGraph.load(new File(modelName), true);
			repaint();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String getAIName() {
		return modelFileName.substring(modelFileName.lastIndexOf('\\') + 1, modelFileName.length());
	}

	/**
	 * Adds a CameraNotFoundListener
	 * 
	 * @param listener the CameraNotFoundListener
	 */
	/*
	 * par Maximilien Harvey
	 */
	public static void addCameraNotFoundListener(CameraNotFoundListener listener) {
		cameraNotFoundListenerList.add(listener);
	}

	/**
	 * Calls the overwritten methods created when adding a listener
	 */
	/*
	 * par Maximilien Harvey
	 */
	public void cameraNotFound() {
		for (CameraNotFoundListener listener : cameraNotFoundListenerList) {
			listener.cameraNotFound();
		}
	}

}
