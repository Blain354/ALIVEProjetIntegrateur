package videofeed;

/**
 * This class contains all the types of video
 * @author Olivier St-Pierre
 *
 */
public enum VideoFeedType {
	OnlyBoxFeed,
	BoxFeed,
	VideoFeed,
	SensorFeed;
}
