package platooning;

import java.awt.Color;


/**
 * This class contains all the type of vehicule we can create
 * @author Olivier St-Pierre
 *
 */
public enum VehicleType {
	BLACKIE(1, "Blackie",0.5, 0.5, Color.BLACK, 0.2),
	GREENIE(2, "Greenie", 0.4, -0.7, Color.GREEN, 0.36),
	ALPHA(3, "Alpha",-0.2,-0.2, Color.RED, 0.2),
	BLUEIE(4,"Blueie",0,0, Color.BLUE, 0.36);

	private String name;
	private int identificationNumber;
	private double originX;
	private double originY;
	private Color color;
	private double maxSpeed;
	/**
	 * Create a new VehiculeType
	 * @param serialNumber the serialNumber of the car
	 * @param name the name of the car
	 */
	VehicleType(int serialNumber, String name, double originX, double originY, Color color, double maxSpeed) {
		this.name = name;
		this.identificationNumber=serialNumber;
		this.originX=originX;
		this.originY=originY;
		this.color=color;
		this.maxSpeed = maxSpeed;

	}

	public int getIdentificationNumber() {
		return identificationNumber;
	}

	public double getOriginX() {
		return originX;
	}
	public double getOriginY() {
		return originY;
	}

	public Color getColor() {
		return color;
	}

	public String getName() {
		return name;
	}
	
	public double getMaxSpeed() {
		return this.maxSpeed;
	}

}
