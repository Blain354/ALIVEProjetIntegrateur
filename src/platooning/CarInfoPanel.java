package platooning;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import aapplication.VehiculeFrame;
import listeners.DecisionTookListener;
import listeners.DistanceChangedListener;

/**
 * this panel is displayed on controlFrame to show a quick overview of the
 * activated car
 * 
 * @author Guillaume Blain
 * @author Olivier St-Pierre
 */
public class CarInfoPanel extends JPanel {
	
	private static final long serialVersionUID = 8321118683502459885L;
	
	private JTable table;
	private LinkedList<String> decisionList = new LinkedList<String>();
	private JLabel lblleft;
	private JLabel lblForward;
	private JLabel lblRight;
	private BufferedImage arrowUp, arrowRight, arrowLeft, arrowDown;
	private Image stopIcon;
	private JLabel lblImageDecision;
	private String lastDecision = "";

	// Translating variables
	private Locale currentLocale;
	private ResourceBundle texts;

	private VehiculeFrame vehicleFrame;

	/**
	 * Create the panel.
	 */
	// Guillaume Blain
	public CarInfoPanel(String name, VehiculeFrame vehiculeFrame) {
		initializeLanguage();

		vehicleFrame = vehiculeFrame;

		setBorder(new LineBorder(new Color(0, 0, 0)));
		setPreferredSize(new Dimension(300, 300));
		setLayout(null);

		lblleft = new JLabel("N/A");
		lblleft.setBounds(23, 86, 69, 20);
		lblleft.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblleft);

		lblRight = new JLabel("N/A");
		lblRight.setBounds(205, 86, 69, 20);
		lblRight.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblRight);

		lblForward = new JLabel("N/A");
		lblForward.setBounds(114, 86, 69, 20);
		lblForward.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblForward);

		JSeparator separator = new JSeparator();
		separator.setBounds(54, 123, 195, 2);
		separator.setForeground(Color.BLUE);
		add(separator);

		JPanel panel = new JPanel();
		panel.setBounds(11, 142, 280, 140);
		panel.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), texts.getString("Decision"), TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		add(panel);
		panel.setLayout(null);

		table = new JTable();
		table.setToolTipText("");
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFont(new Font("Tahoma", Font.PLAIN, 11));
		table.setBorder(new LineBorder(new Color(0, 0, 0)));
		table.setShowVerticalLines(false);
		table.setModel(new DefaultTableModel(
				new Object[][] { { "none" }, { "none" }, { "none" }, { "none" }, { "none" }, { "none" }, },
				new String[] { "Decision" }) {

			
			private static final long serialVersionUID = -2903599213595832301L;

		});
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(81);
		table.setBounds(166, 22, 81, 96);
		panel.add(table);

		lblImageDecision = new JLabel(new ImageIcon("resources/Up.png"));
		lblImageDecision.setBounds(33, 22, 100, 100);
		panel.add(lblImageDecision);

		for (int i = 0; i < 7; i++)
			decisionList.add("N/A");

		JLabel lblNewLabel = new JLabel(name);
		lblNewLabel.setBounds(94, 17, 113, 30);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Arial Black", Font.BOLD, 16));
		add(lblNewLabel);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(54, 64, 195, 5);
		separator_1.setForeground(Color.BLUE);
		add(separator_1);

		try {
			arrowUp = ImageIO.read(new File("resources/Up.png"));
			stopIcon = ImageIO.read(new File("resources/StopIcon.png"));
			stopIcon = stopIcon.getScaledInstance(lblImageDecision.getWidth(), lblImageDecision.getHeight(), Image.SCALE_SMOOTH );
			arrowRight = rotateCw(arrowUp);
			arrowDown = rotateCw(arrowRight);
			arrowLeft = rotateCw(arrowDown);
		} catch (IOException e) {
			e.printStackTrace();
		}
		addAllListener(this.vehicleFrame);

	}

	/**
	 * This method will add all the listener to the vehicule frame
	 * 
	 * @param vehicleFrame the vehiculeFrame that we want to listen
	 */
	// Olivier St-Pierre
	private void addAllListener(VehiculeFrame vehicleFrame) {
		vehicleFrame.addDistanceChangedListener(new DistanceChangedListener() {

			@Override
			public void distanceChanged(int left, int forward, int right) {
				setDistances(left, forward, right);
			}
		});
		vehicleFrame.addDecisionTookListener(new DecisionTookListener() {

			@Override
			public void decisionTook(int decision) {
				addNewestDecision(decision + "");
			}
		});
	}

	/**
	 * add the newest decision to the table
	 * 
	 * @param codeDecision
	 */
	// Guillaume Blain
	public void addNewestDecision(String codeDecision) {

		if (codeDecision != lastDecision) {

			String newDecision = getStringDecision(codeDecision);

			decisionList.addFirst(newDecision);
			for (int i = 0; i < table.getRowCount(); i++) {
				table.setValueAt(decisionList.get(i), i, 0);
			}
			decisionList.removeLast();

		}
		lastDecision = codeDecision;

	}

	/**
	 * It translate a code to a comprehensive String decision type
	 * 
	 * @param code
	 * @return the translated code
	 */
	//Guillaume Blain
	public String getStringDecision(String code) {
		String decisionString = "";
		if (code.charAt(0) == '1') {
			decisionString += texts.getString("Forward");
			lblImageDecision.setIcon(new ImageIcon(arrowUp));
		} else if (code.charAt(0) == '3') {
			decisionString += texts.getString("Backwards");
			lblImageDecision.setIcon(new ImageIcon(arrowDown));

		}
		if (code.length() > 1)
			decisionString += " - ";

		if (code.charAt(code.length() - 1) == '2') {
			decisionString += texts.getString("Right");
			lblImageDecision.setIcon(new ImageIcon(arrowRight));
		} else if (code.charAt(code.length() - 1) == '4') {
			decisionString += texts.getString("Left");
			lblImageDecision.setIcon(new ImageIcon(arrowLeft));
		}

		if (decisionString.equals("")) {
			decisionString += texts.getString("Stop");
			lblImageDecision.setIcon(new ImageIcon(stopIcon));
		}

		return decisionString;
	}

	/**
	 * Set the distances on the label
	 * 
	 * @param left
	 * @param forward
	 * @param right
	 */
	// Guillaume Blain
	public void setDistances(int left, int forward, int right) {
		lblleft.setText(left + " cm");
		lblForward.setText(forward + " cm");
		lblRight.setText(right + " cm");
	}

	/**
	 * this method rotate a bufferedImage clock-wise
	 * 
	 * @param img
	 * @return the rotated image
	 */
	// Guillaume Blain
	public BufferedImage rotateCw(BufferedImage img) {
		int width = img.getWidth();
		int height = img.getHeight();
		BufferedImage newImage = new BufferedImage(height, width, img.getType());

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				newImage.setRGB(height - 1 - j, i, img.getRGB(i, j));
			}
		}
		return newImage;
	}


	/**
	 * This method get the boot language and initialize the language of the
	 * application
	 * 
	 * @throws IOException
	 */
	// Guillaume Blain
	private void initializeLanguage() {
		BufferedReader br = null;
		String lang = "";

		try {
			br = new BufferedReader(new FileReader(new File("resources/languageConfig.txt")));
			lang = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		currentLocale = new Locale(lang);
		texts = ResourceBundle.getBundle("TraductionBundle", currentLocale);
	}
}
