package platooning;

import java.util.LinkedList;
/**
 * This class contains all the methods to manage a platoon and get all the cars' positions
 * @author Olivier St-Pierre
 *
 */
public class Platoon {

	private LinkedList<Vehicle> cars;

	/**
	 * Creates a new platoon
	 */
	public Platoon() {
		cars= new LinkedList<Vehicle>();
	}
	/**
	 * Add a car to the front of the platoon
	 * @param car the new leader of the platoon
	 */
	public void addCarFront(Vehicle car) {
		cars.add(0, car);
	}
	/**
	 * Add a car to the back of the platoon
	 * @param car the last car of the platoon
	 */
	public void addCarBack(Vehicle car) {
		cars.add(car);
	}

	/**
	 * Remove a specific car based on its serial number
	 * @param car the car to remove
	 */
	public void removeCar(Vehicle car) {
		int index = cars.indexOf(car);
		cars.remove(index);
	}
	/**
	 * Return the last car of the platoon
	 * @return the last car
	 */
	public Vehicle getLast() {
		return cars.get(cars.size()-1);
	}
	/**
	 * return the first car of the platoon
	 * @return the leader of the platoon
	 */
	public Vehicle getLeader() {
		return cars.get(0);
	}

	/**
	 * This method is to print the different aspects of the platoon
	 */
	public String toString() {
		String text="";
		for(int i=0; i<cars.size(); i++) {
			text+=cars.get(i).toString()+"\n";
		}
		return text;
	}
}
