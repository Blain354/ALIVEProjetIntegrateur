package platooning;

import java.awt.geom.Ellipse2D;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import aapplication.ControlFrame;
import aapplication.VehiculeFrame;
import mapping.Mapping2D;
import utilities.Utilities;

/**
 * This class contains all the information relatives to a single car
 * 
 * @author Olivier St-Pierre
 *
 */
public class Vehicle {

	private VehicleType vehiculeType;

	private double positionX;
	private double positionY;

	private double timeS;
	private double timeEnd = 0;
	private double time = 0;
	private double MAX_DISTANCE_BROADCAST = 1.0;

	private final int ABBERATE_DISTANCE = 30;
	private final double OBSTACLES_WIDTH = 0.03;
	private ArrayList<Ellipse2D> obstacles = new ArrayList<Ellipse2D>();
	private ArrayList<Vehicle> broadcastVehicles = new ArrayList<Vehicle>();
	private Mapping2D mapping;
	
	private double forwardSquarePosition1;
	private double forwardSquarePosition2;
	private boolean platooningState;
	
	private double[] safety = new double[9];

	private boolean isRolling = false;
	private boolean isReversing = false;

	/**
	 * Create a new Vehicule
	 * 
	 * @param vehiculeType the type of the vehicule
	 */
	public Vehicle(VehicleType vehiculeType) {
		this.vehiculeType = vehiculeType;
		positionX = vehiculeType.getOriginX();
		positionY = vehiculeType.getOriginY();
		this.broadcastVehicles.add(this);

	}

	public int getSerialNumber() {
		return vehiculeType.getIdentificationNumber();
	}

	public String getName() {
		return vehiculeType.getName();
	}

	/**
	 * The method to compare two Vehicule
	 */
	public boolean equals(Object o) {
		Vehicle car = (Vehicle) o;
		return car.getSerialNumber() == vehiculeType.getIdentificationNumber();
	}

	/**
	 * Calculate the position of an obstacle based on the distances from the sensors
	 * 
	 * @param distanceLeft   the distance from the left sensor
	 * @param distanceCenter the distance from the middle sensor
	 * @param distanceRight  the distance from the right sensor
	 * @param angle          the angle in degree
	 */
	public void wallDetection(int distanceLeft, int distanceCenter, int distanceRight, int angle) {
		double posXWall = 0;
		double posYWall = 0;
		double angleRad = Math.toRadians(angle);
		if (distanceLeft < ABBERATE_DISTANCE) {
			posXWall = positionX - distanceLeft / 100.0 * Math.sin(angleRad + Math.toRadians(60));
			posYWall = positionY - distanceLeft / 100.0 * Math.cos(angleRad + Math.toRadians(60));
			addPoint(posXWall, posYWall);
		}

		if (distanceCenter < ABBERATE_DISTANCE) {
			posXWall = positionX - distanceCenter / 100.0 * Math.sin(angleRad);
			posYWall = positionY - distanceCenter / 100.0 * Math.cos(angleRad);
			addPoint(posXWall, posYWall);
		}

		if (distanceRight < ABBERATE_DISTANCE) {
			posXWall = positionX + distanceRight / 100.0 * Math.sin(angleRad + Math.toRadians(60));
			posYWall = positionY + distanceRight / 100.0 * Math.cos(angleRad + Math.toRadians(60));
			addPoint(posXWall, posYWall);
		}

		mapping.repaint();
	}

	/**
	 * Calculate the position of the car
	 * 
	 * @param angle the angle of the car in degree
	 */
	public void trackPosition(int angle) {
		if (isRolling) {

			double angleRad = Math.toRadians(angle);
			if (timeEnd == 0) {
				timeEnd = System.nanoTime();
			}
			time = System.nanoTime() - timeEnd;
			timeS = time * Math.pow(10, -9);
			double distance = this.vehiculeType.getMaxSpeed() * timeS;
			positionX += -distance * Math.sin(angleRad);
			positionY += -distance * Math.cos(angleRad);
			timeEnd = System.nanoTime();
		} else if (isReversing) {

			double angleRad = Math.toRadians(angle);
			if (timeEnd == 0) {
				timeEnd = System.nanoTime();
			}
			time = System.nanoTime() - timeEnd;
			timeS = time * Math.pow(10, -9);
			double distance = -this.vehiculeType.getMaxSpeed() * timeS;
			positionX += -distance * Math.sin(angleRad);
			positionY += -distance * Math.cos(angleRad);
			timeEnd = System.nanoTime();

		} else {
			time = 0;
			timeEnd = 0;
		}

		mapping.repaint();
	}

	/**
	 * Allows the broadcast scan to the nearby cars
	 */
	public void broadcast() {

		ArrayList<Ellipse2D> tempObstacles = new ArrayList<Ellipse2D>();
		long currentTime = System.nanoTime();
		for (VehiculeFrame vf : ControlFrame.vehicleFrameList) {
			if (Utilities.getDistanceBetween2Points(vf.getVehicle().getPositionX(), vf.getVehicle().getPositionY(),
					this.getPositionX(), this.getPositionY()) < MAX_DISTANCE_BROADCAST) {
				tempObstacles.addAll(vf.getVehicle().getObstacles());
				obstacles.addAll(vf.getVehicle().getObstacles());
				broadcastVehicles.add(vf.getVehicle());
				
				//simule extended beacon message
				forwardSquarePosition1=vf.getVehicle().getPositionX();
				forwardSquarePosition2 = vf.getVehicle().getPositionY();
				platooningState=vf.getAiOn();
				
				for(double d:safety) {
					d=vf.getVehicle().getPositionX();
				}

				//*******
				this.obstacles = Utilities.sortWithoutDouble(this.obstacles);

				System.out.println(
						"Delai de V2V Broadcasting pour " + this.getName() + ": " + (System.nanoTime() - currentTime));
				System.out.println(VehiculeFrame.currentTime()*Math.pow(10, -9));
				System.out.println(tempObstacles.size());

				tempObstacles.clear();
			}

		}

		System.out.println();
		mapping.repaint();
	}

	/**
	 * Add a point to the map
	 * 
	 * @param posX the x coordinate of the point
	 * @param posY the y coordinate of the point
	 */
	public void addPoint(double posX, double posY) {
		obstacles.add(new Ellipse2D.Double(posX, posY, OBSTACLES_WIDTH, OBSTACLES_WIDTH));
	}

	/**
	 * This method is to print the different aspects of the car
	 */
	public String toString() {
		return "Name: " + vehiculeType.getName() + "  Serial Number: " + vehiculeType.getIdentificationNumber();
	}

	public double getPositionX() {
		return positionX;
	}

	public double getPositionY() {
		return positionY;
	}

	public void setPositionX(double xOffSet) {
		positionX += xOffSet;
	}

	public void setPositionY(double yOffSet) {
		positionY += yOffSet;
	}

	public VehicleType getVehicleType() {
		return vehiculeType;
	}

	public ArrayList<Ellipse2D> getObstacles() {
		return obstacles;
	}

	public void setRolling(boolean isRolling) {
		this.isRolling = isRolling;
	}

	public void setReversing(boolean isReversing) {
		this.isReversing = isReversing;
	}

	public ArrayList<Vehicle> getBroadcastVehicles() {
		return this.broadcastVehicles;
	}

	public void addMapping(Mapping2D mapping) {
		this.mapping = mapping;
	}

}
