package geometry;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;

/**
 * This class extends from Vector and facilitates it's use to draw vectors.
 * @author Guillaume Blain
 *
 */

public class GraphicVector extends Vector implements Drawable {

	private double origX=0, origY=0;		
	private Line2D.Double lineHead;
	private double angleHead = 0.5;
	private double lengthHead = 20;
	private String labelTag = "";
	private double vectorLength;
	private double vectorAngle;
	private final double MOVE_LABEL = 0.1;
	private Color color = Color.black;

	/**
	 * Basic constructor without any parameter
	 */
	public GraphicVector() {
		super();
	}
	/**
	 * Constructor with two coordinates as center
	 * @param origX X coordinate of the vector origin
	 * @param origY Y coordinate of the vector origin
	 */
	public GraphicVector (double origX, double origY) {
		this.origX = origX;
		this.origY = origY;
		vectorLength = 100;
		vectorAngle = 0;
	}
	/**
	 * Constructor with two coordinates as center and an angle
	 * @param origX X coordinate of the vector origin
	 * @param origY Y coordinate of the vector origin
	 * @param angle The angle of the vector
	 */
	public GraphicVector (double origX, double origY, double angle) {
		this.origX = origX;
		this.origY = origY;
		this.vectorLength = 50;
		this.vectorAngle = angle;
	}
	/**
	 * Constructor with two coordinates as center, an angle and a length
	 * @param origX X coordinate of the vector origin
	 * @param origY Y coordinate of the vector origin
	 * @param length Length of the vector 
	 * @param angle The angle of the vector
	 */
	public GraphicVector (double origX, double origY, double length, double angle) {
		this.origX = origX;
		this.origY = origY;
		this.vectorLength = length;
		this.vectorAngle = angle;
	}
	/**
	 * Method to create the label assigned to the vector
	 * @param g2d Graphic component 
	 */
	private void creerLabel(Graphics2D g2d) {
		g2d.translate(origX + x *(1 + MOVE_LABEL), origY + y * (1 + MOVE_LABEL));
		g2d.drawString(labelTag, 0, 0);
	}

	/**
	 * Draws the vector with a form of an oriented arrow
	 * @param g2d Graphic component
	 */
	public void dessiner(Graphics2D g2d) {	
		Color colorStock = g2d.getColor();
		g2d.setColor(color);
		lineHead = new Line2D.Double(vectorLength -10,0,vectorLength, 0);
		AffineTransform mat = g2d.getTransform();
		g2d.translate(origX, origY);
		Line2D.Double body = new Line2D.Double(0,0,vectorLength, 0);
		g2d.rotate(Math.toRadians(vectorAngle));
		g2d.draw(body);
		g2d.rotate(angleHead/2, vectorLength, 0);
		g2d.draw(lineHead);
		g2d.rotate(-angleHead, vectorLength, 0);
		g2d.draw(lineHead);
		g2d.setTransform(mat);

		creerLabel(g2d);
		g2d.setTransform(mat);
		g2d.setColor(colorStock);
	}

	public void setOrigineXY(double origX, double origY) {
		this.origX = origX;
		this.origY = origY;
	}

	public double getLongueurTete() {
		return lengthHead;
	}

	public void setLongueurTete(double longueurTete) {
		this.lengthHead = longueurTete;
	}

	public void setAngleTete(double angleHead) {
		this.angleHead = angleHead;
	}

	public void setLabel(String label) {
		this.labelTag = label;
	}
/**
 * set the length of the vector and set the color in function of the length
 * @param length
 */
	public void setLength(double length) {
		if(length<=30) {
			setColor(Color.RED);
			this.vectorLength=length;
		} else if(length >30 && length<125) {
			setColor(Color.black);
			this.vectorLength=length;
		} else {
			setColor(Color.green);
			this.vectorLength=125;
		}

	}
	public void setAngle(double angle) {
		this.vectorAngle = angle;
	}

	public void setColor(Color color) {
		this.color = color;
	}


}
