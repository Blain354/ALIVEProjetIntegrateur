package geometry;
import java.awt.Graphics2D;

/**
 * An interface for drawable components which forces them to have a "draw" method
 * @author Guillaume Blain
 *
 */
public interface Drawable {
	public void dessiner( Graphics2D g2d);

}
