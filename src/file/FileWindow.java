package file;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * This window lets users select an image to test the obstacle recognition AI
 * This class was taken from the LRIma project VARC
 * @author Gaya Mehenni
 *
 */
public class FileWindow extends JFrame {


	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JFileChooser fileChooser;
	private String path;

	/**
	 * Constructor
	 */
	public FileWindow() {

		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(0, 0, 600, 400);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		setContentPane(contentPane);

		fileChooser = new JFileChooser();
		//fileChooser.setSelectedFile(new File(getClass().getClassLoader().getResource("").getFile()));
		fileChooser.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					if (!e.getActionCommand().equals(JFileChooser.CANCEL_SELECTION)){
						path = fileChooser.getSelectedFile().getAbsolutePath();
					} else {
						path = "";
					}
				} catch(NullPointerException n) {
					path = "";
				}
				setVisible(false);
			}
		});
		fileChooser.setBounds(0, 0, 400, 400);
		contentPane.add(fileChooser);
	}

	/**
	 * Method that returns the pathfile
	 * @return the pathfile
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Switches the mode for the file chooser
	 * @param mode The mode for the file chooser
	 */
	public void setMode(String mode) {
		if (mode.equals("Folder")) {
			fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		} else if (mode.equals("images")) {
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		}
	}

	/**
	 * Returns the file chooser
	 * @return file chooser
	 */
	public JFileChooser getFileChooser() {
		return fileChooser;
	}

}
