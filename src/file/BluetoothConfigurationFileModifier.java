package file;

import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;


/**
 * this frame is called when the user wants to set the correct com port to his car
 * @author Guillaume Blain
 *
 */
public class BluetoothConfigurationFileModifier extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JComboBox<String> comboBoxComPort;
	private JButton btnSave;
	private ArrayList<String> nameList = new ArrayList<String>();
	private ArrayList<Integer> comPortList = new ArrayList<Integer>();
	private JSpinner spinner;

	// Translating variables
	private static String language = "en";
	private Locale currentLocale;
	private ResourceBundle texts;


	/**
	 * Create the frame.
	 * 
	 * @throws IOException
	 */
	public BluetoothConfigurationFileModifier() throws IOException {
		try {
			initializeLanguageTo(getBootLanguage());
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		ImageIcon icon = new ImageIcon(ImageIO.read(new File("resources\\alivecar.png")));
		setIconImage(icon.getImage());
		setTitle(texts.getString("Configuration"));
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 345, 165);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnHelp = new JMenu(texts.getString("Help"));
		mnHelp.addMenuListener(new MenuListener() {
			public void menuCanceled(MenuEvent e) {
			}
			public void menuDeselected(MenuEvent e) {
			}
			public void menuSelected(MenuEvent e) {
				JFrame helpFrame = new JFrame();
				helpFrame.setBounds(200, 200, 550, 500);
				helpFrame.getContentPane().setLayout(null);
				helpFrame.setResizable(false);
				JLabel message = new JLabel(texts.getString("InfoMessageBluetooth"));
				message.setBounds(10, 0, 510, 50);
				helpFrame.getContentPane().add(message);
				Image imgRead = null;
				Image imgRedim = null;
				double ratio = 0.0;
				try {
					imgRead = ImageIO.read(new File("resources/instructionsBluetooth.png"));
					ratio = (double)imgRead.getWidth(null)/(double)imgRead.getHeight(null);
					
					imgRedim = imgRead.getScaledInstance(550, (int)(550.0/ratio), Image.SCALE_SMOOTH);
					
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				ImageIcon image = new ImageIcon(imgRedim);
				JLabel imageLabel = new JLabel(image);
				imageLabel.setLocation(0, 50);
				imageLabel.setBounds(0, 50, 550, 720);
				helpFrame.getContentPane().add(imageLabel);
				helpFrame.setVisible(true);
			}
		});
		menuBar.add(mnHelp);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		comboBoxComPort = new JComboBox<String>();
		comboBoxComPort.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				int index = comboBoxComPort.getSelectedIndex();
				try {
					spinner.setValue(comPortList.get(index));
				} catch (NullPointerException exception) {
				}
			}
		});
		comboBoxComPort.setBounds(9, 8, 253, 22);
		contentPane.add(comboBoxComPort);

		int counter = 0;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(new File("resources/comPortConfig.txt")));

			String line = "";

			line = br.readLine();
			while (!(line.equals("$"))) {
				nameList.add(counter, line.substring(0, line.indexOf(':')));
				comPortList.add(counter, Integer.parseInt(line.substring(line.indexOf(':') + 1)));
				counter++;
				line = br.readLine();

			}
			for (String s : nameList) {
				comboBoxComPort.addItem(s);
			}
			br.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		comboBoxComPort.setSelectedIndex(0);

		spinner = new JSpinner();
		spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int index = comboBoxComPort.getSelectedIndex();
				comPortList.set(index, (Integer) spinner.getValue());
				reWriteConfigFile();
			}
		});
		spinner.setBounds(271, 9, 58, 22);
		contentPane.add(spinner);
		spinner.setValue(comPortList.get(0));

		JButton btnAddComp = new JButton(texts.getString("AddDevicesMessage"));
		btnAddComp.setBounds(52, 40, 234, 23);
		btnAddComp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFrame addComp = new JFrame();
				addComp.setBounds(200, 200, 250, 110);
				addComp.getContentPane().setLayout(null);
				addComp.setResizable(false);
				JTextArea name = new JTextArea();
				name.setBounds(5,5,150,25);
				addComp.getContentPane().add(name);
				JSpinner port = new JSpinner();
				port.setBounds(160, 5, 70, 25);
				addComp.getContentPane().add(port);
				JButton btnAdd = new JButton(texts.getString("Add"));
				btnAdd.setBounds(70, 40, 100, 25);
				btnAdd.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(!name.getText().equals("")) {
							nameList.add(name.getText());
							comPortList.add((Integer) port.getValue());
							addComp.dispose();
							reWriteConfigFile();
							dispose();
							try {
								new BluetoothConfigurationFileModifier();
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}

						} else {
							JOptionPane.showMessageDialog(null, texts.getString("ValidNameMessage"));
						}
					}
				});
				addComp.getContentPane().add(btnAdd);
				addComp.setVisible(true);
			}
		});
		contentPane.add(btnAddComp);

		btnSave = new JButton(texts.getString("Save"));
		btnSave.setBounds(125, 73, 89, 23);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(null, texts.getString("ConfirmationRebootMessage2"), "",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					System.exit(0);
				} else {
					dispose();
				}
			}
		});
		contentPane.add(btnSave);
		setVisible(true);
	}
	/**
	 * open the the config file and rewrite it with the current list of name/comport
	 */
	public void reWriteConfigFile() {
		try {
			PrintWriter writer = new PrintWriter("resources/comPortConfig.txt");
			for (int i = 0; i < nameList.size(); i++) {
				writer.println(nameList.get(i) + ":" + comPortList.get(i));
			}
			writer.println("$");

			writer.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	/**
	 * This method is used to initialize the language of the application
	 * 
	 * @param lang
	 * @throws IOException
	 */
	private void initializeLanguageTo(String lang) throws IOException {
		language = lang;
		loadLanguage();
	}

	/**
	 * This method loads the good LanguageBundle
	 * 
	 */
	private void loadLanguage() {

		currentLocale = new Locale(language);
		texts = ResourceBundle.getBundle("TraductionBundle", currentLocale);
	}

	/**
	 * This method returns which language is selected which is used when creating the frame
	 * @return the selected language
	 */
	public String getBootLanguage() {
		BufferedReader br = null;
		String lang = "";

		try {
			br = new BufferedReader(new FileReader(new File("resources/languageConfig.txt")));
			lang =  br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lang;
	}

	/**
	 * this method dispose all the active window
	 */
	public void disposeAllWindow() {
		System.gc();
		for (Window window : Window.getWindows()) {
			window.dispose();
		}
	}
}
