package file;

import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
/**
 * This Frame is used to change the language of the platform
 * @author Guilaume Blain
 *
 */
public class LanguageFrame extends JFrame {
	
	private static final long serialVersionUID = 1L;
	// Translating variables
	private Locale currentLocale;
	private ResourceBundle texts;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	public LanguageFrame() throws IOException {
		initializeLanguage();
		ImageIcon icon = new ImageIcon(ImageIO.read(new File("resources\\alivecar.png")));
		setIconImage(icon.getImage());
		setBounds(500, 500, 314, 173);
		setTitle(texts.getString("langaugeSettings"));
		getContentPane().setLayout(null);
		setResizable(false);

		JPanel panelLang = new JPanel();
		panelLang.setBounds(119, 25, 71, 50);
		panelLang.setLayout(new GridLayout(0, 1, 0, 0));

		JRadioButton rdbtnFrancais = new JRadioButton("Francais");
		rdbtnFrancais.setHorizontalAlignment(SwingConstants.LEFT);
		buttonGroup.add(rdbtnFrancais);
		panelLang.add(rdbtnFrancais);

		JRadioButton rdbtnEnglish = new JRadioButton("English");
		rdbtnEnglish.setHorizontalAlignment(SwingConstants.LEFT);
		rdbtnEnglish.setSelected(true);
		buttonGroup.add(rdbtnEnglish);
		panelLang.add(rdbtnEnglish);

		getContentPane().add(panelLang);

		JButton btnSave = new JButton(texts.getString("Save"));
		btnSave.setBounds(92, 85, 125, 25);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(null, texts.getString("ConfirmationRebootMessage"), "",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					if (rdbtnEnglish.isSelected()) {
						setBootLanguage("en");
					} else if (rdbtnFrancais.isSelected()) {
						setBootLanguage("fr");
					}

					System.exit(0);
					
				} else
					dispose();
			}
		});

		getContentPane().add(btnSave);
		setVisible(true);
	}

	/**
	 * This method is used to change the language of the application
	 * 
	 * @throws IOException
	 */
	private void initializeLanguage() {
		currentLocale = new Locale(getBootLanguage());
		texts = ResourceBundle.getBundle("TraductionBundle", currentLocale);
	}

	/**
	 * This method returns which language is selected which is used when creating
	 * the frame
	 * 
	 * @return the selected language
	 */
	public String getBootLanguage() {
		BufferedReader br = null;
		String lang = "";

		try {
			br = new BufferedReader(new FileReader(new File("resources/languageConfig.txt")));
			lang = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lang;
	}
	/**
	 * This method set the boot language
	 * @param lang the new boot language
	 */
	public void setBootLanguage(String lang) {
		try {
			PrintWriter writer = new PrintWriter("resources/languageConfig.txt");
			writer.print(lang);
			writer.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * this method dispose all the active window
	 * @throws IOException 
	 */
	public void disposeAllWindow() throws IOException {
		System.gc();
		for (Window window : Window.getWindows()) {
			//if(!window.equals(JFrame.getWindows()[0]))
			//window.dispose();
			window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
			//else
			
		}
	}

}
