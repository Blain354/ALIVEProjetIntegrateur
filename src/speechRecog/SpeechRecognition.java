package speechRecog;

import java.util.ArrayList;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;
import listeners.VoiceDecisionListener;
/**
 * This class allows to control the car using voice recognition
 * Commands are : Up, Down, Right, Left, Stop
 * @author �mile Gagn�
 *
 */
public class SpeechRecognition extends Thread{

	private Configuration configuration;
	private boolean isAnalyzing = true;
	private SpeechResult result;
	private String command;
	private ArrayList<VoiceDecisionListener> voiceDecisionListenerList = new ArrayList<VoiceDecisionListener>();
	private int decision;
	public SpeechRecognition() {
		configuration = new Configuration();
		// Set path to acoustic model.
		configuration.setAcousticModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us");
		// Set path to dictionary.
		configuration.setDictionaryPath("resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict");

		// Set language model.
		//configuration.setLanguageModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us.lm.bin");

		// Set the grammar.
		configuration.setGrammarPath("resources/");
		configuration.setGrammarName("grammar");
		configuration.setUseGrammar(true);
	}

	@Override
	public void run() {

		try {
			LiveSpeechRecognizer recognizer = new LiveSpeechRecognizer(configuration);
			// Start recognition process pruning previously cached data.
			//recognizer.startRecognition(true);
			while(true) {
				if(isAnalyzing) {
					result = recognizer.getResult();
					command = result.getHypothesis();
					System.out.println("Commande vocale : " + command);
					if(command.equals("up")) {
						decision = 1;
						voiceDecision();
					}else if(command.contains("down")) {
						decision = 3;
						voiceDecision();
					}else if(command.contains("left")) {
						decision = 4;
						voiceDecision();
					}else if(command.contains("right")) {
						decision = 2;
						voiceDecision();
					}else if(command.contains("stop")) {
						decision = 0;
						voiceDecision();
					}
				}
			}
			// Pause recognition process. It can be resumed then with startRecognition(false).
			// recognizer.stopRecognition();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getCommand() {
		return this.result.getHypothesis();
	}

	public void addVoiceDecisionListener(VoiceDecisionListener voiceDecisionListener) {
		voiceDecisionListenerList.add(voiceDecisionListener);
	}
	
	public void voiceDecision() {
		for (VoiceDecisionListener listener : voiceDecisionListenerList) {
			listener.voiceDecision(this.decision);
		}
	}
}
