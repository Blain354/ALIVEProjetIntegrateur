package dataset;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import file.FileWindow;
import videofeed.DisplayImage;

/**
 * This program is used to capture image from the camera and redirect them into
 * a local file on the computer. This file filled with raw footage will be used
 * to create a data set by annotating each frame on another software like
 * labelImg -- https://tzutalin.github.io/labelImg/ --
 * 
 * @author Guillaume Blain
 *
 */
public class CameraToFile extends JFrame implements Runnable {

	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnRecord;
	private JButton btnPause;
	private DisplayImage imagePanel;
	private JLabel lblCurrentImage;
	private JPanel panel;

	private Thread recording;
	private boolean isRecording = false;
	BufferedImage bfImage = null;
	int i = getCurrentImage();
	String savingFilepath = getSavingFilePath();

	// Translating variables
	private Locale currentLocale;
	private ResourceBundle texts;
	private JMenuItem mntmCurrentNumberImage;
	private JMenu mnHelp;
	private JMenuItem mntmNewMenuItem;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CameraToFile frame = new CameraToFile();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws IOException
	 */
	public CameraToFile() throws IOException {
		initializeLanguage();
		setTitle(texts.getString("CameraToFileTitle"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 488);

		FileWindow saveAs = new FileWindow();
		saveAs.addWindowListener(new WindowListener() {
			public void windowClosing(WindowEvent e) {}

			public void windowOpened(WindowEvent e) {}

			public void windowClosed(WindowEvent e) {}

			public void windowIconified(WindowEvent e) {}

			public void windowDeiconified(WindowEvent e) {}

			public void windowActivated(WindowEvent e) {}

			public void windowDeactivated(WindowEvent e) {
				savingFilepath = saveAs.getPath() + "\\";
				setSavingFilePath(savingFilepath);
			}

		});

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnOption = new JMenu(texts.getString("Options"));
		menuBar.add(mnOption);

		JMenuItem mntmSaving = new JMenuItem(texts.getString("Saving"));
		mntmSaving.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveAs.setMode("Folder");
				saveAs.setVisible(true);
			}
		});
		mnOption.add(mntmSaving);

		mntmCurrentNumberImage = new JMenuItem(texts.getString("CurrentNumberImage"));
		mntmCurrentNumberImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				JFrame currentImgFrame = new JFrame();
				currentImgFrame.setResizable(false);
				currentImgFrame.setBounds(200, 200, 300, 200);
				currentImgFrame.getContentPane().setLayout(null);
				JLabel lblnbImage = new JLabel(texts.getString("newIndexImageMess"));
				lblnbImage.setBounds(5,5,290,25);
				lblnbImage.setHorizontalAlignment(SwingConstants.CENTER);
				currentImgFrame.getContentPane().add(lblnbImage);
				JSpinner indexImg = new JSpinner();
				indexImg.setBounds(11,41,130,25);
				currentImgFrame.getContentPane().add(indexImg);
				JButton btnSave = new JButton(texts.getString("Save"));
				btnSave.setBounds(152,41,130,25);
				btnSave.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						setCurrentImage((int) indexImg.getValue());
						currentImgFrame.dispose();
						CameraToFile.main(null);
					}
				});
				currentImgFrame.getContentPane().add(btnSave);
				currentImgFrame.setVisible(true);

			}
		});
		mnOption.add(mntmCurrentNumberImage);

		mnHelp = new JMenu(texts.getString("Help"));
		menuBar.add(mnHelp);

		mntmNewMenuItem = new JMenuItem(texts.getString("showCurrentFile"));
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File file = new File (getSavingFilePath());
				Desktop desktop = Desktop.getDesktop();
				try {
					desktop.open(file);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mnHelp.add(mntmNewMenuItem);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 30));

		btnRecord = new JButton(texts.getString("Record"));
		btnRecord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnPause.setEnabled(true);
				btnRecord.setEnabled(false);
				start();
			}
		});
		panel.add(btnRecord, BorderLayout.WEST);

		btnPause = new JButton(texts.getString("Pause"));
		btnPause.setEnabled(false);
		btnPause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnPause.setEnabled(false);
				btnRecord.setEnabled(true);
				stop();

			}
		});
		panel.add(btnPause, BorderLayout.CENTER);
		JLabel lblNewLabel = new JLabel(texts.getString("Currentimagenumber"));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNewLabel, BorderLayout.NORTH);

		lblCurrentImage = new JLabel(getCurrentImage() + "");
		lblNewLabel.setLabelFor(lblCurrentImage);
		lblCurrentImage.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblCurrentImage, BorderLayout.EAST);

		JPanel panelImage = new JPanel();
		SpringLayout sl_contentPane = new SpringLayout();
		sl_contentPane.putConstraint(SpringLayout.EAST, panel, -5, SpringLayout.EAST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.NORTH, panelImage, 5, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, panelImage, 0, SpringLayout.WEST, panel);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, panelImage, -5, SpringLayout.NORTH, panel);
		sl_contentPane.putConstraint(SpringLayout.EAST, panelImage, 0, SpringLayout.EAST, panel);
		sl_contentPane.putConstraint(SpringLayout.NORTH, panel, -93, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, panel, 5, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, panel, -5, SpringLayout.SOUTH, contentPane);
		contentPane.setLayout(sl_contentPane);
		contentPane.add(panel);
		contentPane.add(panelImage);
		panelImage.setLayout(new BorderLayout(0, 0));

		imagePanel = new DisplayImage();
		panelImage.add(imagePanel, BorderLayout.CENTER);

	}

	
	public void run() {
		while (isRecording) {

			URL url = null;
			try {
				url = new URL("http://192.168.0.162/capture");
				bfImage = ImageIO.read(url);
				Thread.sleep(5);
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
			i++;
			setCurrentImage(i);
			System.out.println(i);
			imagePanel.setImage(bfImage);
			lblCurrentImage.setText(i + "");
			try {
				ImageIO.write(bfImage, "jpg", new File(savingFilepath + i + ".jpg"));
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * Start the thread
	 */
	public void start() {
		if (isRecording == false) {
			recording = new Thread(this);
			recording.start();
		}
		isRecording = true;
	}

	/**
	 * Stop the thread
	 */
	public void stop() {
		isRecording = false;
	}

	/**
	 * this method is used at the beginning of the program to get what was the last
	 * frame number
	 * 
	 * @return the current image
	 */
	public int getCurrentImage() {
		BufferedReader br = null;
		String currentImg = "";
		try {
			br = new BufferedReader(new FileReader(new File("resources/currentImage.txt")));
			currentImg = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Integer.parseInt(currentImg);
	}

	/**
	 * This method set the current image
	 * 
	 * @param currentImg the new current image
	 */
	public void setCurrentImage(int currentImg) {
		try {
			PrintWriter writer = new PrintWriter("resources/currentImage.txt");
			writer.print(currentImg);
			writer.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * This method is used to change the language of the application
	 * 
	 * @throws IOException
	 */
	private void initializeLanguage() {
		currentLocale = new Locale(getBootLanguage());
		texts = ResourceBundle.getBundle("TraductionBundle", currentLocale);
	}

	/**
	 * This method returns which language is selected which is used when creating
	 * the frame
	 * 
	 * @return the selected language
	 */
	public String getBootLanguage() {
		BufferedReader br = null;
		String lang = "";

		try {
			br = new BufferedReader(new FileReader(new File("resources/languageConfig.txt")));
			lang = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lang;
	}

	/**
	 * This method set the saving file path
	 * 
	 * @param filePath the new file path
	 */
	public void setSavingFilePath(String filePath) {
		try {
			PrintWriter writer = new PrintWriter("resources/SavingAsFilePath.txt");
			writer.print(filePath);
			writer.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	/**
	 * this method is used to get the last file path saved by the user
	 * 
	 * @return the file path where to save image
	 */
	public String getSavingFilePath() {
		BufferedReader br = null;
		String filePath = "";

		try {
			br = new BufferedReader(new FileReader(new File("resources/SavingAsFilePath.txt")));
			filePath = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return filePath;
	}

}
