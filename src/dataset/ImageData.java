package dataset;

import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 * This class contains an image with a square in it
 * @author Olivier St-Pierre
 *
 */
public class ImageData {
	private BufferedImage image;

	private int posXT;
	private int posYT;

	private int posXB;
	private int posYB;

	private final int WIDTH=480;
	private final int HEIGHT=360;

	private final int MAX_WIDTH = 160;
	private final int MIN_WIDTH = 80;
	private final double MIN_RATIO = 0.8;
	private final double MAX_RATIO = 1.2;

	private final int ALPHA = 255;

	private final int DECALX = 1;
	private final int DECALY = 1;

	private final int RED_MAX=255;
	private final int RED_MIN=196;
	private final int GB_MAX=190;
	private final int GB_MIN=130;

	/**
	 * Create a neW ImageData
	 */
	public ImageData() {
		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
		generateImage();
	}

	/**
	 * Generate an image from zero
	 */
	private void generateImage() {
		for(int i = 0; i<WIDTH; i+=DECALX) {
			for(int j = 0; j<HEIGHT; j+=DECALY) {
				int rgb = calculateRGB(randomizeColor());
				for(int k =0; k<DECALX; k++) {
					for(int l = 0; l<DECALY; l++) {
						image.setRGB(i+k, j+l, rgb);
					}
				}

			}
		}
		randomizePosition();
		drawSquare(calculateRGB(randomizeSquareColor()));

	}

	/**
	 * Calculate the rgb for the noise around the square
	 * @param rgb the rgb value
	 * @return a Color
	 */
	private int calculateRGB(int[] rgb) {
		Color color = new Color(rgb[0], rgb[1], rgb[2], ALPHA);
		return color.getRGB();
	}

	/**
	 * This method randomize the position of the square
	 */
	private void randomizePosition() {

		posXT= (int) (Math.random()*WIDTH);
		posYT = (int) (Math.random()*HEIGHT);

		int width = (int) (Math.random()*(MAX_WIDTH-MIN_WIDTH)+MIN_WIDTH);
		double ratio = Math.random()*(MAX_RATIO-MIN_RATIO)+MIN_RATIO;

		if(posXT+width<=WIDTH) {
			posXB = posXT+width;
		}else {
			posXB=WIDTH;
		}
		if(posYT+width*ratio<=HEIGHT) {
			posYB = (int) (posYT+width*ratio);
		}else {
			posYB=HEIGHT;
		}

	}

	/**
	 * Draw the square on the image
	 * @param rgb the color of the square
	 */
	private void drawSquare(int rgb) {
		for(int i = posXT; i<posXB; i++) {
			for(int j = posYT; j<posYB; j++) {
				image.setRGB(i, j, rgb);
			}
		}
	}

	/**
	 * Randomize the color for the noise
	 * @return the value of the rgb
	 */
	private int[] randomizeColor() {
		int rgb[] = new int[3];
		rgb[0] =  (int) (Math.random()*195);
		int green =160;
		while(green>=130&&green<=190) {
			green = (int) (Math.random()*255);
		}
		rgb[1]= green;

		int blue =160;
		while(blue>=130&&blue<=190) {
			blue = (int) (Math.random()*255);
		}
		rgb[2]= blue;
		return rgb;

	}

	/**
	 * Randomize the color of the square
	 * @return
	 */
	private int[] randomizeSquareColor() {
		int rgb[] = new int[3];
		rgb[0] = (int) (Math.random()*(RED_MAX-RED_MIN)+RED_MIN);
		rgb[1] = (int) (Math.random()*(GB_MAX-GB_MIN)+GB_MIN);
		rgb[2] = (int) (Math.random()*(GB_MAX-GB_MIN)+GB_MIN);
		return rgb;
	}

	public BufferedImage getImage() {
		return image;
	}

	public int getPosXT() {
		return posXT;
	}

	public int getPosYT() {
		return posYT;
	}

	public int getPosXB() {
		return posXB;
	}

	public int getPosYB() {
		return posYB;
	}

	public int getWIDTH() {
		return WIDTH;
	}

	public int getHEIGHT() {
		return HEIGHT;
	}


}
