package dataset;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Robot;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;

import com.sun.glass.events.KeyEvent;

/**
 * This Program is useful for labeling image on LabelImg   ---https://tzutalin.github.io/labelImg/---
 * 
 * 1.Start the java program
 * 2.Setup the labeling 
 * 3.If you have more than one
 * object, enable "Single Class Mode" or Ctrl + Shift + S and simply annotate
 * the whole dataset for each object (This way, you know roughly where the items
 * you're looking for are and its much simpler to program haha) 
 * 4.Press 'P' to
 * enable or disable the helping program
 * 		 i.When activated, just use your mouse
 * 			to select the object 
 * 		ii.If no object is on the frame, simply press space bar
 *			but if its the second time you're through the dataset, simply press 'D'
 * 
 * 
 * 'P' --> Enable/disable 
 * SPACE bar --> Save and skip 
 * 'D' --> Skip
 * 'O' --> Enable/disable Auto-Skip Mode
 * 
 * @author Guillaume Blain
 *
 */
public class LabelImgHelper {

	private static boolean isReadyToAnnotate = false;
	private static boolean isAutoSkipMode = false;
	private static int sleepTime = 5;
	private static int mode = 1;
	private static int nbClick = 0;

	public static void main(String[] args) {
		try {
			GlobalScreen.registerNativeHook();
		} catch (NativeHookException ex) {
			System.exit(1);
		}

		GlobalScreen.addNativeKeyListener(new NativeKeyListener() {

			@Override
			public void nativeKeyTyped(NativeKeyEvent e) {}
			@Override
			public void nativeKeyReleased(NativeKeyEvent e) {}
			@Override
			public void nativeKeyPressed(NativeKeyEvent e) {
				System.out.println("Key Pressed: " + NativeKeyEvent.getKeyText(e.getKeyCode()));

				if (e.getKeyCode() == NativeKeyEvent.VC_ESCAPE) {
					try {
						GlobalScreen.unregisterNativeHook();
					} catch (NativeHookException e1) {
						e1.printStackTrace();
					}
				}

				if (e.getKeyCode() == NativeKeyEvent.VC_1) {

					if (isReadyToAnnotate) {
						mode = 1;
						pressW();

					}
				}

				if (e.getKeyCode() == NativeKeyEvent.VC_2) {

					if (isReadyToAnnotate) {
						mode = 2;
						pressW();

					}
				}
				if (e.getKeyCode() == NativeKeyEvent.VC_3) {

					if (isReadyToAnnotate) {
						mode = 3;
						pressW();

					}
				}

				if (e.getKeyCode() == NativeKeyEvent.VC_P) {
					isReadyToAnnotate = !isReadyToAnnotate;
					if (isReadyToAnnotate) {
						pressW();
					}
				}

				if (e.getKeyCode() == NativeKeyEvent.VC_SPACE) {
					if (isReadyToAnnotate) {
						pressD();
						pressW();
					}

				}

				if (e.getKeyCode() == NativeKeyEvent.VC_O) {

					isAutoSkipMode = !isAutoSkipMode;
				}

			}
		});
		GlobalScreen.addNativeMouseListener(new NativeMouseListener() {

			@Override
			public void nativeMouseReleased(NativeMouseEvent e) {
				if (isReadyToAnnotate) {
					nbClick++;

					if (nbClick == mode) {
						pressD();
						pressW();
						nbClick = 0;

						if (isAutoSkipMode) {
							try {
								Thread.sleep(10);
							} catch (InterruptedException e1) {
								e1.printStackTrace();
							}
							while (isGreen()) {
								pressD();
								try {
									Thread.sleep(75);
								} catch (InterruptedException e1) {
									e1.printStackTrace();
								}
							}
						}

					} else {
						pressW();
					}
				}
			}
			public void nativeMousePressed(NativeMouseEvent e) {}
			public void nativeMouseClicked(NativeMouseEvent e) {}
		});
	}
/**
 * pause the program
 * @param millis
 */
	public static void wait(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * press D on the keyboard
	 */
	public static void pressD() {
		Robot r = null;
		try {
			r = new Robot();
		} catch (AWTException e1) {
			e1.printStackTrace();
		}

		wait(sleepTime);

		r.keyPress(KeyEvent.VK_D);
		r.keyRelease(KeyEvent.VK_D);
	}

	/**
	 * press W on the keyboard
	 */
	public static void pressW() {
		Robot r = null;
		try {
			r = new Robot();
		} catch (AWTException e1) {
			e1.printStackTrace();
		}

		wait(sleepTime);

		r.keyPress(KeyEvent.VK_W);
		r.keyRelease(KeyEvent.VK_W);
	}

	/**
	 * it is used to check if a specific pixel on the screen is a specific green
	 * @return true or false if a specific pixel on the screen is a specific green
	 */
	public static boolean isGreen() {
		Robot r = null;
		Color color = null;
		try {
			r = new Robot();
			color = r.getPixelColor(150, 150);
		} catch (AWTException e) {
			e.printStackTrace();
		}
		if (color.getRed() == 212 && color.getGreen() == 240 && color.getBlue() == 139)
			return true;
		else
			return false;

	}

}
