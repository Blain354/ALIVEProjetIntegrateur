package dataset;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import javax.imageio.ImageIO;

/**
 * This class contains a main that creates the number of image you want
 * @author Olivier St-Pierre
 *
 */
public class PinkSquareDataSetCreator {

	private static String pathImage = "C:\\Users\\e1836636\\Desktop\\DataSet\\DataSetImage\\";
	private static String pathAnnotation = "C:\\Users\\e1836636\\Desktop\\DataSet\\Annotations\\";
	private static String mainFile = "C:\\Users\\e1836636\\Desktop\\DataSet";
	private static int reference = 10000;


	public static void main(String[] args) {
		createImages(reference, pathImage);
	}

	/**
	 * Create a number of image and write them to the files
	 * @param reference the number of images
	 * @param path the path to where it is write
	 */
	private static void createImages(int reference, String path) {
		File file;
		ImageData image;
		for(int i =0; i<reference; i++) {
			image = new ImageData();
			try {

				file = new File(path+i+".jpg");
				ImageIO.write(image.getImage(), "png", file);
				writeXML(i+".jpg", image);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * This method creates a custom xml file for an image
	 * @param imageName the name of the image
	 * @param image the image itself
	 */
	private static void writeXML(String imageName, ImageData image) {
		try {
			File fileOut = new File(pathAnnotation+reference+".xml");
			FileOutputStream outStream = new FileOutputStream(fileOut);

			BufferedWriter bWriter = new BufferedWriter(new OutputStreamWriter(outStream));

			bWriter.write("<annotation>");
			bWriter.newLine();

			bWriter.write("	<folder>"+mainFile+"</folder>");
			bWriter.newLine();

			bWriter.write("	<filename>"+imageName+"</filename>");
			bWriter.newLine();

			bWriter.write("	<source>"+"</source>");
			bWriter.newLine();

			bWriter.write("	<size>");
			bWriter.newLine();

			bWriter.write("		<width>"+image.getWIDTH()+"</width>"); 
			bWriter.newLine();

			bWriter.write("		<height>"+image.getHEIGHT()+"</height>"); 
			bWriter.newLine();

			bWriter.write("		<depth>"+3+"</depth>"); 
			bWriter.newLine();

			bWriter.write("	</size>");
			bWriter.newLine();

			bWriter.write("	<segmented>"+"0"+"</segmented>");
			bWriter.newLine();

			bWriter.write("	<object>");
			bWriter.newLine();


			bWriter.write("		<name>"+"Square"+"</name>");
			bWriter.newLine();

			bWriter.write("		<pose>"+"Unspecified"+"</pose>");
			bWriter.newLine();

			bWriter.write("		<truncated>"+"Unspecified"+"</truncated>");
			bWriter.newLine();

			bWriter.write("		<difficult>"+"Unspecified"+"</difficult>");
			bWriter.newLine();

			bWriter.write("		<bndbox>");
			bWriter.newLine();	

			bWriter.write("			<xmin>"+image.getPosXT()+"</xmin>");
			bWriter.newLine();
			bWriter.write("			<ymin>"+image.getPosYT()+"</ymin>");
			bWriter.newLine();
			bWriter.write("			<xmax>"+image.getPosXB()+"</xmax>");
			bWriter.newLine();
			bWriter.write("			<ymax>"+image.getPosYB()+"</ymax>");
			bWriter.newLine();

			bWriter.write("		</bndbox>");
			bWriter.newLine();

			bWriter.write("	</object>");
			bWriter.newLine();	

			bWriter.write("</annotation>");
			bWriter.newLine();

			bWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
