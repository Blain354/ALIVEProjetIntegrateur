package ai;

import static org.bytedeco.opencv.global.opencv_core.CV_8U;
import static org.bytedeco.opencv.global.opencv_imgproc.FONT_HERSHEY_DUPLEX;
import static org.bytedeco.opencv.global.opencv_imgproc.putText;
import static org.bytedeco.opencv.global.opencv_imgproc.rectangle;
import static org.bytedeco.opencv.global.opencv_imgproc.resize;
import static org.bytedeco.opencv.helper.opencv_core.RGB;

import java.io.File;
import java.util.List;
import java.util.Random;

import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.opencv.opencv_core.Point;
import org.bytedeco.opencv.opencv_core.Scalar;
import org.bytedeco.opencv.opencv_core.Size;
import org.datavec.api.records.metadata.RecordMetaDataImageURI;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.objdetect.ObjectDetectionRecordReader;
import org.datavec.image.recordreader.objdetect.impl.VocLabelProvider;
import org.deeplearning4j.common.resources.DL4JResources;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.ConvolutionMode;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.WorkspaceMode;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.objdetect.Yolo2OutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.layers.objdetect.DetectedObject;
import org.deeplearning4j.nn.transferlearning.FineTuneConfiguration;
import org.deeplearning4j.nn.transferlearning.TransferLearning;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.CollectScoresIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.deeplearning4j.zoo.model.TinyYOLO;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.nd4j.linalg.learning.config.RmsProp;

import listeners.CustomScoreIterationListener;

/**
 * Creates, trains and evaluates an object detection AI. It is used to recognize
 * dangers on the road Based heavily on the SVHN house number recognition
 * example provided by dl4j
 * 
 * @author Maximilien Harvey
 *
 */
public class ObjectDetectionAI {

	// Box colors for evaluation
	public static final Scalar RED = RGB(255.0, 0, 0);
	public static final Scalar GREEN = RGB(0, 255.0, 0);
	public static final Scalar BLUE = RGB(0, 0, 255.0);
	public static final Scalar YELLOW = RGB(255.0, 255.0, 0);
	public static final Scalar CYAN = RGB(0, 255.0, 255.0);
	public static final Scalar MAGENTA = RGB(255.0, 0.0, 255.0);
	public static final Scalar ORANGE = RGB(255.0, 128.0, 0);
	public static final Scalar PINK = RGB(255.0, 192.0, 203.0);
	public static final Scalar LIGHTBLUE = RGB(153.0, 204.0, 255.0);
	public static final Scalar VIOLET = RGB(238.0, 130.0, 238.0);

	public static void main(String[] args) throws java.lang.Exception {
		DL4JResources.setBaseDownloadURL("https://dl4jdata.blob.core.windows.net/");
		// parameters matching the pretrained TinyYOLO model
		int width = 320;
		int height = 240;
		int nChannels = 3;
		int gridWidth = 10;
		int gridHeight = 8;

		// classes (objects) for the Berkeley dataset
		int nClasses = 1;

		// parameters for the Yolo2OutputLayer
		int nBoxes = 5;
		double lambdaNoObj = 0.5;
		double lambdaCoord = 1.0;
		double[][] priorBoxes = { { 2, 5 }, { 2.5, 6 }, { 3, 7 }, { 3.5, 8 }, { 4, 9 } };
		//double detectionThreshold = 0.5;

		// parameters for the training phase
		int batchSize = 5;
		int nEpochs = 2;
		double learningRate = 1e-4;
		int seed = 123;
		Random rng = new Random(seed);

		// Directory for the test and training datasets

		String dataDir = "D:\\Documents\\PinkSquare";
		File trainDir = new File(dataDir, "Data");
		File testDir = new File(dataDir, "Bloc 51");

		/*RandomPathFilter pathFilter = new RandomPathFilter(rng) {
			@Override
			protected boolean accept(String name) {
				name = name.replace("/images/", "/annotations/").replace(".jpg", ".xml");
				// System.out.println("Name " + name);
				try {
					return new File(new URI(name)).exists();
				} catch (URISyntaxException ex) {
					throw new RuntimeException(ex);
				}
			}
		};*/

		/*InputSplit[] totalData = new FileSplit(trainDir, NativeImageLoader.ALLOWED_FORMATS, rng).sample(pathFilter, 0.6,
				0.4);
		InputSplit trainingData = totalData[0];
		InputSplit testingData = totalData[1];*/

		FileSplit trainData = new FileSplit(trainDir, NativeImageLoader.ALLOWED_FORMATS, rng);
		FileSplit testData = new FileSplit(testDir, NativeImageLoader.ALLOWED_FORMATS, rng);

		// Record readers to provide labels for each image
		System.out.println("on commence");
		ObjectDetectionRecordReader recordReaderTrain = new ObjectDetectionRecordReader(height, width, nChannels,
				gridHeight, gridWidth, new VocLabelProvider(dataDir));
		recordReaderTrain.initialize(trainData);

		// [bike, bus, car, motor, person, rider, traffic light, traffic sign, train,
		// truck]

		ObjectDetectionRecordReader recordReaderTest = new ObjectDetectionRecordReader(height, width, nChannels,
				gridHeight, gridWidth, null);
		try {
			recordReaderTest.initialize(testData);
		} catch (Exception e) {

		}
		System.out.println("Data read");

		// Iterators to go through each dataset
		RecordReaderDataSetIterator train = new RecordReaderDataSetIterator(recordReaderTrain, batchSize, 1, 1, true);
		train.setPreProcessor(new ImagePreProcessingScaler(0, 1));

		RecordReaderDataSetIterator test = null;
		try {
			test = new RecordReaderDataSetIterator(recordReaderTest, 1, 1, 1, true);
			test.setPreProcessor(new ImagePreProcessingScaler(0, 1));
		} catch (Exception e) {

		}

		ComputationGraph model;

		String modelFilename = "C:\\Users\\maxim\\git\\ALIVEProjetIntegrateur\\AIPinkSquare.zip";

		if (new File(modelFilename).exists()) {
			System.out.println("Model found");
			// CollectScoresIterationListener collectScores = new
			// CollectScoresIterationListener();

			model = ComputationGraph.load(new File(modelFilename), true);
			/*model.setListeners(new CustomScoreIterationListener(1));
			model.fit(train, nEpochs);
			// collectScores.exportScores(new
			// File("C:\\Users\\maxim\\OneDrive\\Documents\\ScoresPourGraphiques\\ScoreTinyYolo5.txt"));
			try {
				ModelSerializer.writeModel(model,
						"C:\\Users\\maxim\\OneDrive\\Documents\\AI\\TinyYoloResolution1280_17_newdata_16.zip", true);
			} catch (Exception e) {
				ModelSerializer.writeModel(model, "TinyYoloResolution1280_17_newdata_16.zip", true);
			}*/

		} else {

			System.out.println("Creating model");

			/*
			 * We use TinyYOLO (You Only Look Once), a pretrained model that serves object
			 * detection purposes We tune the model to work better for our data
			 */
			ComputationGraph pretrained = (ComputationGraph) TinyYOLO.builder().build().initPretrained();
			INDArray priors = Nd4j.create(priorBoxes);

			FineTuneConfiguration fineTuneConf = new FineTuneConfiguration.Builder().seed(seed)
					.optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
					.gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
					.gradientNormalizationThreshold(1.0).updater(new RmsProp(learningRate))
					// .updater(new
					// Nesterovs.Builder().learningRate(learningRate).momentum(lrMomentum).build())
					.activation(Activation.IDENTITY).miniBatch(true).trainingWorkspaceMode(WorkspaceMode.ENABLED)
					.inferenceWorkspaceMode(WorkspaceMode.ENABLED).build();

			model = new TransferLearning.GraphBuilder(pretrained).fineTuneConfiguration(fineTuneConf)
					.setInputTypes(InputType.convolutional(height, width, nChannels))
					.removeVertexKeepConnections("conv2d_9").removeVertexKeepConnections("outputs")
					.addLayer("convolution2d_9",
							new ConvolutionLayer.Builder(1, 1).nIn(1024).nOut(nBoxes * (5 + nClasses)).stride(1, 1)
							.convolutionMode(ConvolutionMode.Same).weightInit(WeightInit.UNIFORM)
							.activation(Activation.IDENTITY).build(),
							"leaky_re_lu_8")
					/*
					 * .addLayer("batch_normalization_9", new
					 * BatchNormalization.Builder(1,1).nIn(2048).nOut(2048)
					 * .weightInit(WeightInit.XAVIER).activation(Activation.IDENTITY).build(),
					 * "convolution2d_9") .addLayer("convolution2d_10", new
					 * ConvolutionLayer.Builder(1, 1).nIn(2048).nOut(nBoxes * (5 +
					 * nClasses)).stride(1, 1)
					 * .convolutionMode(ConvolutionMode.Same).weightInit(WeightInit.XAVIER)
					 * .activation(Activation.IDENTITY).build(), "batch_normalization_9")
					 */
					.addLayer("outputs",
							new Yolo2OutputLayer.Builder().lambdaNoObj(lambdaNoObj).lambdaCoord(lambdaCoord)
							.boundingBoxPriors(priors).build(),
							"convolution2d_9")
					.setOutputs("outputs").build();
			System.out.println(model.summary(InputType.convolutional(height, width, nChannels)));

			System.out.println("Train model...");

			CollectScoresIterationListener collectScores = new CollectScoresIterationListener();

			model.setListeners(collectScores, new CustomScoreIterationListener(1));
			long timeBefore = System.currentTimeMillis();
			model.fit(train, nEpochs);
			collectScores
			.exportScores(new File("C:\\Users\\maxim\\OneDrive\\Documents\\ScoresPourGraphiques\\1280.txt"));
			System.out.println(System.currentTimeMillis() - timeBefore);

			System.out.println("Save model...");
			try {
				ModelSerializer.writeModel(model, "C:\\Users\\maxim\\OneDrive\\Documents\\AI\\" + modelFilename, true);
			} catch (Exception e) {
				ModelSerializer.writeModel(model, modelFilename, true);
			}
		}
		NativeImageLoader imageLoader = new NativeImageLoader();
		CanvasFrame frame = new CanvasFrame("HouseNumberDetection");
		OpenCVFrameConverter.ToMat converter = new OpenCVFrameConverter.ToMat();
		org.deeplearning4j.nn.layers.objdetect.Yolo2OutputLayer yout = (org.deeplearning4j.nn.layers.objdetect.Yolo2OutputLayer) model
				.getOutputLayer(0);
		List<String> labels = train.getLabels();
		train.setCollectMetaData(true);
		Scalar[] colormap = { RED, BLUE, GREEN, CYAN, YELLOW, MAGENTA, ORANGE, PINK, LIGHTBLUE, VIOLET };

		while (train.hasNext() && frame.isVisible()) {
			org.nd4j.linalg.dataset.DataSet ds = train.next();
			RecordMetaDataImageURI metadata = (RecordMetaDataImageURI) ds.getExampleMetaData().get(0);
			INDArray featuresTemp = ds.getFeatures();
			INDArray features = featuresTemp.get(NDArrayIndex.interval(2, 3));
			//INDArray features = featuresTemp.get(NDArrayIndex.interval(2, 3));
			INDArray results = model.outputSingle(features);
			List<DetectedObject> objs = yout.getPredictedObjects(results, 0.5);
			File file = new File(metadata.getURI());
			System.out.println(file.getName() + ": " + objs);

			Mat mat = imageLoader.asMat(features);
			Mat convertedMat = new Mat();
			mat.convertTo(convertedMat, CV_8U, 255, 0);
			int w = metadata.getOrigW() * 2;
			int h = metadata.getOrigH() * 2;
			Mat image = new Mat();
			resize(convertedMat, image, new Size(w, h));
			for (DetectedObject obj : objs) {
				double[] xy1 = obj.getTopLeftXY();
				double[] xy2 = obj.getBottomRightXY();
				String label = labels.get(obj.getPredictedClass());
				int x1 = (int) Math.round(w * xy1[0] / gridWidth);
				int y1 = (int) Math.round(h * xy1[1] / gridHeight);
				int x2 = (int) Math.round(w * xy2[0] / gridWidth);
				int y2 = (int) Math.round(h * xy2[1] / gridHeight);
				rectangle(image, new Point(x1, y1), new Point(x2, y2), colormap[obj.getPredictedClass()]);
				putText(image, label, new Point(x1 + 2, y2 - 2), FONT_HERSHEY_DUPLEX, 1,
						colormap[obj.getPredictedClass()]);

			}
			frame.setTitle(new File(metadata.getURI()).getName() + " - HouseNumberDetection");
			frame.setCanvasSize(w, h);
			frame.showImage(converter.convert(image));
			frame.waitKey();
		}
		frame.dispose();
	}

}
