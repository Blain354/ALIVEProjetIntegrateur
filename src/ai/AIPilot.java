package ai;

import java.io.IOException;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import listeners.DistanceChangedListener;
import platooning.Vehicle;
import threads.GetData;
/**
 * This class is used to load the trained AI and feed it the data gathered from the car sensors. 
 * It's sole purpose is to analyze that data and give an answer according to it.
 * @author �mile Gagn�
 */
public class AIPilot{

	private final double SCALER = 300.0;
	private MultiLayerNetwork MLN;
	private INDArray data, newData;
	private int[] response;
	private int angle;
	private GetData getData;
	private boolean aiState = false;
	/**
	 * Constructor without Mapping2D
	 * @param outStream BT outStream
	 * @param inStream BT inStream
	 * @param MLN The MultiLayerNetwork associated
	 * @throws IOException When the I/O encounters a problem
	 */
	public AIPilot(GetData getData, MultiLayerNetwork MLN ) throws IOException {
		this.MLN = MLN;
		this.getData = getData;
		this.getData.addDistanceChangedListener(new DistanceChangedListener() {

			@Override
			public void distanceChanged(int left, int forward, int right) throws IOException {
				if(aiState) {
					System.out.println("Right : " + response[2] + " Forward : " + response[0] + " Left : " + response[1]);
					int result = makeDecision(response[1],response[0],response[2]);
					afficherResultat(result);
					if(result == 2) {
						getData.getOutputStream().write(2);
					}else if(result == 1) {
						getData.getOutputStream().write(4);
					}else {
						getData.getOutputStream().write(1);
					}
					getData.getOutputStream().flush();
				}
			}
		});
	}
	/**
	 * Constructor with Mapping2D
	 * @param outStream BT outStream
	 * @param inStream BT inStream
	 * @param MLN The MultiLayerNetwork associated
	 * @param map The Mapping2D associated
	 * @throws IOException When the I/O encounters a problem
	 */
	public AIPilot(GetData getData, MultiLayerNetwork MLN,Vehicle vehicle ) throws IOException {
		this.MLN = MLN;
		this.getData = getData;
		this.getData.addDistanceChangedListener(new DistanceChangedListener() {

			@Override
			public void distanceChanged(int left, int forward, int right) throws IOException {
				if(aiState) {
					System.out.println("Right : " + right + " Forward : " + forward + " Left : " + left);
					int result = makeDecision(left,forward,right);
					afficherResultat(result);
					if(result == 2) {
						getData.getOutputStream().write(2);
					}else if(result == 1) {
						getData.getOutputStream().write(4);
					}else {
						getData.getOutputStream().write(1);
					}
					getData.getOutputStream().flush();
				}
			}
		});
	}

	/**
	 * This method handles the decision making of the AI based on the data sensors
	 * @param d Distance on the right of the vehicle
	 * @param a Distance in front of the vehicle
	 * @param g Distance on the left of the vehicle
	 * @return Returns the index of the array with the highest percentage (Which represents the right decision)
	 */
	public int makeDecision(double d, double a, double g) {
		data = Nd4j.zeros(1,3);
		data.putScalar(0, d/SCALER);
		data.putScalar(1, a/SCALER);
		data.putScalar(2, g/SCALER);
		newData = MLN.output(data);
		double decision = 0;
		int decisionIndex = 3;
		for(int i = 0; i < newData.length(); i++) {
			if(newData.getDouble(i) > decision) {
				decision = newData.getDouble(i);
				decisionIndex = i;
			}
		}
		return decisionIndex;
	}
	/**
	 * This method displays the result in a more readable way
	 * @param result This parameter is the integer result given by the neural network
	 */
	public static void afficherResultat(int result) {
		System.out.print("AI Result : ");
		switch(result) {
		case 1:
			System.out.println("Left");
			break;
		case 2:
			System.out.println("Right");
			break;
		case 3:
			System.out.println("Forward");
			break;
		}
	}

	/**
	 * Getter for the angle
	 * @return The angle as a int
	 */
	public int getAngle() {
		return angle;
	}
	/**
	 * Setter for the state of the Ai
	 * @param state The Ai state
	 */
	public void setAiState(boolean state) {
		aiState = state;
	}
}
