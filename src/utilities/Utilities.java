package utilities;

import java.awt.geom.Ellipse2D;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * A class that contains all the methods that can be useful in multiples classes
 * @author Olivier St-Pierre
 *
 */
public class Utilities {

	/**
	 *   Calculate the angle with the good sign
	 *    @param angle the angle between 0 and 180
	 *    @param spin the number of half spin
	 *    @param positive the direction of the spins
	 *    @return the angle in degree
	 *
	 */
	public static int calculateAngle(int angle, int spin, int positive) {
		if(positive ==1) {
			return (angle+(spin*180))%360;
		}else {
			return -1*((angle+(spin*180))%360);

		}
	}

	/** 
	 * Test if a value is between or equal to two bounds
	 *@param value the value to test
	 *@param min minimum of the range
	 *@param max the maximum of the range
	 *@return true if the value is within range false if not
	 *
	 */
	public static boolean isInRange(double value, double min, double max) {
		return value>=min&&value<=max;
	}

	/** 
	 * Take a decision base on where the square of the front car is
	 *@param value the x coordinate of the center of the square
	 *@param min the minimum of the range
	 *@param max the max of the range
	 *@return 0 if left, 1 if in front, 2 if right
	 * 
	 */
	public static int detectFrontCar(double value, double min, double max) {
		if(isInRange(value, min, max)) {
			
			return 0;
		}else if(value<min) {
			return 14;
		}else {
			return 12;
		}
	}
	/**
	 * This method is used to round numbers by a specific numbers of decimal
	 * 
	 * @param value the value to round
	 * @param places the number of decimals
	 * @return the rounded value 
	 */
	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(Double.toString(value));
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
	/**
	 * This method calculates the distance between 2 points
	 * @param x1 X coordinate of the first point
	 * @param y1 Y coordinate of the first point
	 * @param x2 X coordinate of the second point
	 * @param y2 Y coordinate of the second point
	 * @return The distance between the 2 points
	 */
	public static double getDistanceBetween2Points(double x1, double y1, double x2, double y2) {
		return Math.sqrt(Math.pow(x2-x1, 2)+Math.pow(y2-y1, 2));
	}
	/**
	 * Gets rid of duplicates in an ArrayList
	 * @param al The arrayList
	 * @return The sorted arrayList without duplicates
	 */
	public static ArrayList<Ellipse2D> sortWithoutDouble(ArrayList<Ellipse2D> al) {
		return (ArrayList<Ellipse2D>) al.stream().distinct().collect(Collectors.toList());
	}
}
