package utilities;

import java.awt.geom.AffineTransform;

/**
 * This class creates a matrix of transformation to go from the real world to the pixel
 * @author Olivier St-Pierre
 *
 */
public class DisplayModel {

	private double heightRealUnit = -1;
	private double pixelsPerUnitX;
	private double pixelsPerUnitY;
	private AffineTransform matRP;

	/**
	 * Create a new conversion matrix
	 * @param widthPixels the width of the component
	 * @param heightPixels the height of the component
	 * @param widthRealUnit the width of the real world to simulate
	 */
	public DisplayModel(double widthPixels, double heightPixels, double widthRealUnit) {
		this.heightRealUnit = widthRealUnit * heightPixels/widthPixels;

		this.pixelsPerUnitX = widthPixels/widthRealUnit;
		this.pixelsPerUnitY = heightPixels/heightRealUnit;

		AffineTransform mat = new AffineTransform();
		mat.scale(pixelsPerUnitX, pixelsPerUnitY);

		this.matRP = mat;
	}

	/**
	 * Return the matrix to transform to the pixel from the real world
	 * @return an AffineTransform with the conversion matrix
	 */
	public AffineTransform getModel() {
		return matRP;
	}

	public double getHeightRealUnit() {
		return heightRealUnit;
	}

	public double getPixelsPerUnitX() {
		return pixelsPerUnitX;
	}
}
